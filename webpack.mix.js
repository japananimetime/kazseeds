const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js")
    .js("resources/js/minor.js", "public/js")
    .js("resources/js/personalLink.js", "public/js")
    .js("resources/js/home.js", "public/js")
    .js("resources/js/catalog.js", "public/js")
    .js("resources/js/card.js", "public/js")
    .js("resources/js/personal.js", "public/js")
    .js("resources/js/updatepass.js", "public/js")
    .js("resources/js/about.js", "public/js")
    .js("resources/js/contacts.js", "public/js")
    .js("resources/js/news.js", "public/js")
    .js("resources/js/services.js", "public/js")
    .js("resources/js/cart.js", "public/js")
    .sass("resources/sass/app.scss", "public/css")
    .sass("resources/sass/pages/home.scss", "public/css")
    .sass("resources/sass/pages/catalog.scss", "public/css")
    .sass("resources/sass/pages/card.scss", "public/css")
    .sass("resources/sass/pages/personal.scss", "public/css")
    .sass("resources/sass/pages/about.scss", "public/css")
    .sass("resources/sass/pages/contacts.scss", "public/css")
    .sass("resources/sass/pages/news.scss", "public/css")
    .sass("resources/sass/pages/services.scss", "public/css")
    .sass("resources/sass/pages/cart.scss", "public/css");
