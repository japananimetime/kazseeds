<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/product/{id}/{local?}', 'FrontController@Product')->name('Product');
Route::get('/personal/{local?}', 'FrontController@Personal')->name('Personal');
Route::get('/about/{local?}', 'FrontController@About')->name('About');
Route::get('/contacts/{local?}', 'FrontController@Contacts')->name('Contacts');
Route::get('/news/{local?}', 'FrontController@News')->name('News');
Route::get('/services/{local?}', 'FrontController@Services')->name('Services');

Route::get('/forget/{email?}/{hashed?}/{local?}','FrontController@Forget')->name('Forget');

Route::get('products/{local?}', 'FrontController@Products')->name('Products');
Route::get('questionnaire','FrontController@Questionnaire')->name('questionnaire');

Route::get('kazseeds/php_info',function(){
    phpinfo();
});
Route::get('cart/{local?}', 'FrontController@Cart')->name('Cart');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/{local?}', 'FrontController@Index')->name('Index');
