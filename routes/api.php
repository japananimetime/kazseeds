<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::apiResource('/catalog','ProductController');
Route::post('/login','AuthController@Login');
Route::post('/register','AuthController@Register');
Route::post('/search','ProductController@SearchProduct');
Route::get('/basket','ProductController@Basket');
Route::post('/order','ProductController@Order');
Route::post('/email','AuthController@EmailCheck');
Route::post('/phone','AuthController@PhoneCheck');
Route::get('/token','AuthController@TokenCheck');
Route::get('/user/order','ProductController@UserOrder');
Route::get('questions','OrderController@GetQuestions');
Route::get('/order/history','OrderController@History');
Route::post('test/completed','OrderController@TestCompleted');
Route::post('/request/order','OrderController@Request');
Route::post('forget','AuthController@forgetPassword');
Route::post('update','AuthController@updatePassword');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
