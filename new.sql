--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: agronomy_services; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.agronomy_services (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.agronomy_services OWNER TO postgres;

--
-- Name: agronomy_services_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.agronomy_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agronomy_services_id_seq OWNER TO postgres;

--
-- Name: agronomy_services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.agronomy_services_id_seq OWNED BY public.agronomy_services.id;


--
-- Name: answers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.answers (
    id bigint NOT NULL,
    answer text NOT NULL,
    answer_kz text NOT NULL,
    answer_ru text NOT NULL,
    question_id bigint NOT NULL,
    count bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.answers OWNER TO postgres;

--
-- Name: answers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.answers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.answers_id_seq OWNER TO postgres;

--
-- Name: answers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.answers_id_seq OWNED BY public.answers.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categories (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.categories OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- Name: data_rows; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.data_rows (
    id integer NOT NULL,
    data_type_id integer NOT NULL,
    field character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    display_name character varying(255) NOT NULL,
    required boolean DEFAULT false NOT NULL,
    browse boolean DEFAULT true NOT NULL,
    read boolean DEFAULT true NOT NULL,
    edit boolean DEFAULT true NOT NULL,
    add boolean DEFAULT true NOT NULL,
    delete boolean DEFAULT true NOT NULL,
    details text,
    "order" integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.data_rows OWNER TO postgres;

--
-- Name: data_rows_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.data_rows_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_rows_id_seq OWNER TO postgres;

--
-- Name: data_rows_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.data_rows_id_seq OWNED BY public.data_rows.id;


--
-- Name: data_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.data_types (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    display_name_singular character varying(255) NOT NULL,
    display_name_plural character varying(255) NOT NULL,
    icon character varying(255),
    model_name character varying(255),
    description character varying(255),
    generate_permissions boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    server_side smallint DEFAULT '0'::smallint NOT NULL,
    controller character varying(255),
    policy_name character varying(255),
    details text
);


ALTER TABLE public.data_types OWNER TO postgres;

--
-- Name: data_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.data_types_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_types_id_seq OWNER TO postgres;

--
-- Name: data_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.data_types_id_seq OWNED BY public.data_types.id;


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: featured_products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.featured_products (
    id bigint NOT NULL,
    product_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.featured_products OWNER TO postgres;

--
-- Name: featured_products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.featured_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.featured_products_id_seq OWNER TO postgres;

--
-- Name: featured_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.featured_products_id_seq OWNED BY public.featured_products.id;


--
-- Name: menu_items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.menu_items (
    id integer NOT NULL,
    menu_id integer,
    title character varying(255) NOT NULL,
    url character varying(255) NOT NULL,
    target character varying(255) DEFAULT '_self'::character varying NOT NULL,
    icon_class character varying(255),
    color character varying(255),
    parent_id integer,
    "order" integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    route character varying(255),
    parameters text
);


ALTER TABLE public.menu_items OWNER TO postgres;

--
-- Name: menu_items_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.menu_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menu_items_id_seq OWNER TO postgres;

--
-- Name: menu_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.menu_items_id_seq OWNED BY public.menu_items.id;


--
-- Name: menus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.menus (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.menus OWNER TO postgres;

--
-- Name: menus_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.menus_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menus_id_seq OWNER TO postgres;

--
-- Name: menus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.menus_id_seq OWNED BY public.menus.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: news; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.news (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    text text NOT NULL,
    additional_text text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.news OWNER TO postgres;

--
-- Name: news_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_id_seq OWNER TO postgres;

--
-- Name: news_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.news_id_seq OWNED BY public.news.id;


--
-- Name: news_media; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.news_media (
    id bigint NOT NULL,
    news_id bigint NOT NULL,
    media_path text NOT NULL,
    type character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT news_media_type_check CHECK (((type)::text = ANY ((ARRAY['main_media'::character varying, 'other_media'::character varying])::text[])))
);


ALTER TABLE public.news_media OWNER TO postgres;

--
-- Name: news_media_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.news_media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_media_id_seq OWNER TO postgres;

--
-- Name: news_media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.news_media_id_seq OWNED BY public.news_media.id;


--
-- Name: order_products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.order_products (
    id bigint NOT NULL,
    order_id bigint NOT NULL,
    manager_id bigint NOT NULL,
    product_id bigint NOT NULL,
    quantity integer NOT NULL,
    dimension character varying(255) NOT NULL,
    total_product_price bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.order_products OWNER TO postgres;

--
-- Name: order_products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.order_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_products_id_seq OWNER TO postgres;

--
-- Name: order_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.order_products_id_seq OWNED BY public.order_products.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orders (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    user_id bigint,
    email character varying(255),
    phone character varying(255),
    content text NOT NULL,
    total bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.orders OWNER TO postgres;

--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_id_seq OWNER TO postgres;

--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;


--
-- Name: pages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pages (
    id integer NOT NULL,
    author_id integer NOT NULL,
    title character varying(255) NOT NULL,
    excerpt text,
    body text,
    image character varying(255),
    slug character varying(255) NOT NULL,
    meta_description text,
    meta_keywords text,
    status character varying(255) DEFAULT 'INACTIVE'::character varying NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT pages_status_check CHECK (((status)::text = ANY ((ARRAY['ACTIVE'::character varying, 'INACTIVE'::character varying])::text[])))
);


ALTER TABLE public.pages OWNER TO postgres;

--
-- Name: pages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pages_id_seq OWNER TO postgres;

--
-- Name: pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pages_id_seq OWNED BY public.pages.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- Name: permission_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permission_role (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.permission_role OWNER TO postgres;

--
-- Name: permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    key character varying(255) NOT NULL,
    table_name character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.permissions OWNER TO postgres;

--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO postgres;

--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- Name: posts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.posts (
    id integer NOT NULL,
    author_id integer NOT NULL,
    category_id integer,
    title character varying(255) NOT NULL,
    seo_title character varying(255),
    excerpt text,
    body text NOT NULL,
    image character varying(255),
    slug character varying(255) NOT NULL,
    meta_description text,
    meta_keywords text,
    status character varying(255) DEFAULT 'DRAFT'::character varying NOT NULL,
    featured boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT posts_status_check CHECK (((status)::text = ANY ((ARRAY['PUBLISHED'::character varying, 'DRAFT'::character varying, 'PENDING'::character varying])::text[])))
);


ALTER TABLE public.posts OWNER TO postgres;

--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.posts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.posts_id_seq OWNER TO postgres;

--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.posts_id_seq OWNED BY public.posts.id;


--
-- Name: product_media; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_media (
    id bigint NOT NULL,
    product_id bigint NOT NULL,
    type character varying(255) NOT NULL,
    image_path text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT product_media_type_check CHECK (((type)::text = ANY ((ARRAY['main_image'::character varying, 'other_images'::character varying])::text[])))
);


ALTER TABLE public.product_media OWNER TO postgres;

--
-- Name: product_media_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_media_id_seq OWNER TO postgres;

--
-- Name: product_media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_media_id_seq OWNED BY public.product_media.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    category_id bigint NOT NULL,
    price bigint NOT NULL,
    description text NOT NULL,
    keywords text,
    zone_of_maturation character varying(255),
    region character varying(255),
    female_line character varying(255),
    male_line character varying(255),
    silk_ugd character varying(255),
    black_ugd character varying(255),
    agronomic_chars text,
    cob_chars text,
    introduction_guide text,
    manager_id bigint NOT NULL,
    subcategory_id bigint NOT NULL,
    dimension character varying(255) DEFAULT 'kg'::character varying NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT products_dimension_check CHECK (((dimension)::text = ANY ((ARRAY['kg'::character varying, 'tonne'::character varying, 'centner'::character varying, 'grams'::character varying])::text[])))
);


ALTER TABLE public.products OWNER TO postgres;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO postgres;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- Name: questions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.questions (
    id bigint NOT NULL,
    question text NOT NULL,
    question_kz text NOT NULL,
    question_ru text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.questions OWNER TO postgres;

--
-- Name: questions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.questions_id_seq OWNER TO postgres;

--
-- Name: questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.questions_id_seq OWNED BY public.questions.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    display_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.settings (
    id integer NOT NULL,
    key character varying(255) NOT NULL,
    display_name character varying(255) NOT NULL,
    value text,
    details text,
    type character varying(255) NOT NULL,
    "order" integer DEFAULT 1 NOT NULL,
    "group" character varying(255)
);


ALTER TABLE public.settings OWNER TO postgres;

--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.settings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_id_seq OWNER TO postgres;

--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: sliders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sliders (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    image_path text NOT NULL,
    link character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.sliders OWNER TO postgres;

--
-- Name: sliders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sliders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sliders_id_seq OWNER TO postgres;

--
-- Name: sliders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sliders_id_seq OWNED BY public.sliders.id;


--
-- Name: subcategories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.subcategories (
    id bigint NOT NULL,
    category_id bigint NOT NULL,
    title character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.subcategories OWNER TO postgres;

--
-- Name: subcategories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.subcategories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subcategories_id_seq OWNER TO postgres;

--
-- Name: subcategories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.subcategories_id_seq OWNED BY public.subcategories.id;


--
-- Name: translations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.translations (
    id integer NOT NULL,
    table_name character varying(255) NOT NULL,
    column_name character varying(255) NOT NULL,
    foreign_key integer NOT NULL,
    locale character varying(255) NOT NULL,
    value text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.translations OWNER TO postgres;

--
-- Name: translations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.translations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.translations_id_seq OWNER TO postgres;

--
-- Name: translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.translations_id_seq OWNED BY public.translations.id;


--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_roles (
    user_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.user_roles OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    phone character varying(255),
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    avatar character varying(255) DEFAULT 'users/default.png'::character varying,
    role_id bigint,
    settings text
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: agronomy_services id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agronomy_services ALTER COLUMN id SET DEFAULT nextval('public.agronomy_services_id_seq'::regclass);


--
-- Name: answers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answers ALTER COLUMN id SET DEFAULT nextval('public.answers_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- Name: data_rows id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_rows ALTER COLUMN id SET DEFAULT nextval('public.data_rows_id_seq'::regclass);


--
-- Name: data_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_types ALTER COLUMN id SET DEFAULT nextval('public.data_types_id_seq'::regclass);


--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: featured_products id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.featured_products ALTER COLUMN id SET DEFAULT nextval('public.featured_products_id_seq'::regclass);


--
-- Name: menu_items id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu_items ALTER COLUMN id SET DEFAULT nextval('public.menu_items_id_seq'::regclass);


--
-- Name: menus id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menus ALTER COLUMN id SET DEFAULT nextval('public.menus_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: news id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news ALTER COLUMN id SET DEFAULT nextval('public.news_id_seq'::regclass);


--
-- Name: news_media id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_media ALTER COLUMN id SET DEFAULT nextval('public.news_media_id_seq'::regclass);


--
-- Name: order_products id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_products ALTER COLUMN id SET DEFAULT nextval('public.order_products_id_seq'::regclass);


--
-- Name: orders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


--
-- Name: pages id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pages ALTER COLUMN id SET DEFAULT nextval('public.pages_id_seq'::regclass);


--
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- Name: posts id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.posts ALTER COLUMN id SET DEFAULT nextval('public.posts_id_seq'::regclass);


--
-- Name: product_media id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_media ALTER COLUMN id SET DEFAULT nextval('public.product_media_id_seq'::regclass);


--
-- Name: products id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- Name: questions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questions ALTER COLUMN id SET DEFAULT nextval('public.questions_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: sliders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sliders ALTER COLUMN id SET DEFAULT nextval('public.sliders_id_seq'::regclass);


--
-- Name: subcategories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subcategories ALTER COLUMN id SET DEFAULT nextval('public.subcategories_id_seq'::regclass);


--
-- Name: translations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.translations ALTER COLUMN id SET DEFAULT nextval('public.translations_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: agronomy_services; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.agronomy_services (id, title, created_at, updated_at) FROM stdin;
1	Variety selection	2020-07-15 04:46:14	2020-07-15 04:46:14
2	Herbicide plans	2020-07-15 04:46:22	2020-07-15 04:46:22
3	Crop rotation	2020-07-15 04:46:30	2020-07-15 04:46:30
4	Tillage recommendations	2020-07-15 04:46:37	2020-07-15 04:46:37
5	Fertility recommendations	2020-07-15 04:46:44	2020-07-15 04:46:44
6	Harvest recommendations	2020-07-15 04:46:51	2020-07-15 04:46:51
7	Equipment settings	2020-07-15 04:46:59	2020-07-15 04:46:59
8	Troubleshooting	2020-07-15 04:47:05	2020-07-15 04:47:05
9	In-season visits	2020-07-15 04:47:11	2020-07-15 04:47:11
10	Phone and email consultations	2020-07-15 04:47:18	2020-07-15 04:47:18
\.


--
-- Data for Name: answers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.answers (id, answer, answer_kz, answer_ru, question_id, count, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categories (id, title, created_at, updated_at) FROM stdin;
1	Зерна	2020-07-15 03:56:31	2020-07-15 03:56:31
2	Крупы	2020-07-16 04:52:34	2020-07-16 04:52:34
\.


--
-- Data for Name: data_rows; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.data_rows (id, data_type_id, field, type, display_name, required, browse, read, edit, add, delete, details, "order") FROM stdin;
118	12	id	text	Id	t	f	f	f	f	f	{}	1
119	12	title	text	Title	t	t	t	t	t	t	{}	5
120	12	category_id	text	Category Id	t	t	t	t	t	t	{}	2
121	12	price	text	Price	t	t	t	t	t	t	{}	6
122	12	description	text	Description	t	t	t	t	t	t	{}	7
123	12	keywords	text	Keywords	f	t	t	t	t	t	{}	8
124	12	zone_of_maturation	text	Zone Of Maturation	f	t	t	t	t	t	{}	9
125	12	region	text	Region	f	t	t	t	t	t	{}	10
126	12	female_line	text	Female Line	f	t	t	t	t	t	{}	11
127	12	male_line	text	Male Line	f	t	t	t	t	t	{}	12
128	12	silk_ugd	text	Silk Ugd	f	t	t	t	t	t	{}	13
129	12	black_ugd	text	Black Ugd	f	t	t	t	t	t	{}	14
130	12	agronomic_chars	text	Agronomic Chars	f	t	t	t	t	t	{}	15
131	12	cob_chars	text	Cob Chars	f	t	t	t	t	t	{}	16
132	12	introduction_guide	text	Introduction Guide	f	t	t	t	t	t	{}	17
133	12	manager_id	text	Manager Id	t	t	t	t	t	t	{}	3
134	12	subcategory_id	text	Subcategory Id	t	t	t	t	t	t	{}	4
135	12	dimension	text	Dimension	t	t	t	t	t	t	{}	18
136	12	created_at	timestamp	Created At	f	t	t	t	f	t	{}	19
137	12	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	20
138	1	id	text	Id	t	f	f	f	f	f	{}	1
139	1	name	text	Name	t	t	t	t	t	t	{}	3
140	1	email	text	Email	t	t	t	t	t	t	{}	4
141	1	phone	text	Phone	f	t	t	t	t	t	{}	5
142	1	email_verified_at	timestamp	Email Verified At	f	t	t	t	t	t	{}	6
143	1	password	text	Password	t	t	t	t	t	t	{}	7
144	1	remember_token	text	Remember Token	f	t	t	t	t	t	{}	8
145	1	created_at	timestamp	Created At	f	t	t	t	f	t	{}	9
146	1	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	10
147	1	avatar	text	Avatar	f	t	t	t	t	t	{}	11
148	1	role_id	text	Role Id	f	t	t	t	t	t	{}	2
149	1	settings	text	Settings	f	t	t	t	t	t	{}	12
150	3	id	text	Id	t	f	f	f	f	f	{}	1
151	3	name	text	Name	t	t	t	t	t	t	{}	2
152	3	display_name	text	Display Name	t	t	t	t	t	t	{}	3
153	3	created_at	timestamp	Created At	f	t	t	t	f	t	{}	4
154	3	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	5
155	4	id	text	Id	t	f	f	f	f	f	{}	1
156	4	title	text	Title	t	t	t	t	t	t	{}	2
157	4	image_path	text	Image Path	t	t	t	t	t	t	{}	3
158	4	link	text	Link	t	t	t	t	t	t	{}	4
159	4	created_at	timestamp	Created At	f	t	t	t	f	t	{}	5
160	4	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	6
\.


--
-- Data for Name: data_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.data_types (id, name, slug, display_name_singular, display_name_plural, icon, model_name, description, generate_permissions, created_at, updated_at, server_side, controller, policy_name, details) FROM stdin;
2	menus	menus	Menu	Menus	voyager-list	TCG\\Voyager\\Models\\Menu		t	2020-07-14 11:01:43	2020-07-14 11:01:43	0		\N	\N
5	categories	categories	Категория товара	Категории товаров	voyager-sort	App\\Category	\N	t	2020-07-15 03:50:07	2020-07-15 03:50:07	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"desc","default_search_key":null}
7	agronomy_services	agronomy-services	Agronomy Service	Agronomy Services	voyager-hammer	App\\AgronomyService	\N	t	2020-07-15 04:45:51	2020-07-15 04:45:51	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}
8	news	news	Новость	Новости	\N	App\\News	\N	t	2020-07-15 06:14:23	2020-07-15 06:28:56	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
14	featured_products	featured-products	Рекомендованные продукты	Рекомендованный продукт	voyager-heart	App\\FeaturedProduct	\N	t	2020-07-15 10:20:35	2020-07-15 10:22:51	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
15	subcategories	subcategories	Подкатегория	Подкатегории	\N	App\\Subcategory	\N	t	2020-07-20 04:20:35	2020-07-20 04:20:35	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}
11	product_media	product-media	Product Medium	Product Media	\N	App\\ProductMedia	\N	t	2020-07-15 09:28:13	2020-07-22 10:28:34	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
9	news_media	news-media	Медиа для новостей	Фото для новостей	voyager-images	App\\NewsMedia	\N	t	2020-07-15 06:15:31	2020-07-23 06:18:47	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
12	products	products	Product	Products	\N	App\\Product	\N	t	2020-07-15 09:29:13	2020-07-23 10:22:15	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
1	users	users	User	Users	voyager-person	TCG\\Voyager\\Models\\User	\N	t	2020-07-14 11:01:43	2020-07-23 11:40:36	0	TCG\\Voyager\\Http\\Controllers\\VoyagerUserController	TCG\\Voyager\\Policies\\UserPolicy	{"order_column":null,"order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}
3	roles	roles	Role	Roles	voyager-lock	TCG\\Voyager\\Models\\Role	\N	t	2020-07-14 11:01:43	2020-07-23 11:41:06	0	TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController	\N	{"order_column":null,"order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}
4	sliders	sliders	Слайдер	Слайдеры	voyager-list	App\\Slider	\N	t	2020-07-14 11:22:10	2020-07-23 11:42:16	0	\N	\N	{"order_column":"id","order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}
\.


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.failed_jobs (id, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- Data for Name: featured_products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.featured_products (id, product_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: menu_items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.menu_items (id, menu_id, title, url, target, icon_class, color, parent_id, "order", created_at, updated_at, route, parameters) FROM stdin;
\.


--
-- Data for Name: menus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.menus (id, name, created_at, updated_at) FROM stdin;
1	admin	2020-07-14 11:01:43	2020-07-14 11:01:43
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
6	2020_07_23_105652_create_questions_table	3
7	2020_07_23_105704_create_answers_table	3
29	2020_07_15_033760_create_subcategories_table	1
104	2014_10_12_000000_create_users_table	2
105	2014_10_12_100000_create_password_resets_table	2
106	2016_01_01_000000_add_voyager_user_fields	2
107	2016_01_01_000000_create_data_types_table	2
108	2016_01_01_000000_create_pages_table	2
109	2016_01_01_000000_create_posts_table	2
110	2016_05_19_173453_create_menu_table	2
111	2016_10_21_190000_create_roles_table	2
112	2016_10_21_190000_create_settings_table	2
113	2016_11_30_135954_create_permission_table	2
114	2016_11_30_141208_create_permission_role_table	2
115	2016_12_26_201236_data_types__add__server_side	2
116	2017_01_13_000000_add_route_to_menu_items_table	2
117	2017_01_14_005015_create_translations_table	2
118	2017_01_15_000000_make_table_name_nullable_in_permissions_table	2
119	2017_03_06_000000_add_controller_to_data_types_table	2
120	2017_04_11_000000_alter_post_nullable_fields_table	2
121	2017_04_21_000000_add_order_to_data_rows_table	2
122	2017_07_05_210000_add_policyname_to_data_types_table	2
123	2017_08_05_000000_add_group_to_settings_table	2
124	2017_11_26_013050_add_user_role_relationship	2
125	2017_11_26_015000_create_user_roles_table	2
126	2018_03_11_000000_add_user_settings	2
127	2018_03_14_000000_add_details_to_data_types_table	2
128	2018_03_16_000000_make_settings_value_nullable	2
129	2019_08_19_000000_create_failed_jobs_table	2
130	2020_07_14_110947_create_sliders_table	2
131	2020_07_15_033757_create_categories_table	2
132	2020_07_15_033758_create_subcategories_table	2
133	2020_07_15_033857_create_products_table	2
134	2020_07_15_044331_create_agronomy_services_table	2
135	2020_07_15_052921_create_product_media_table	2
136	2020_07_15_060539_create_news_table	2
137	2020_07_15_060650_create_news_media_table	2
138	2020_07_15_101615_create_featured_products_table	2
139	2020_07_23_074111_create_orders_table	2
140	2020_07_23_074242_create_order_products_table	2
\.


--
-- Data for Name: news; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.news (id, title, text, additional_text, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: news_media; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.news_media (id, news_id, media_path, type, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: order_products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.order_products (id, order_id, manager_id, product_id, quantity, dimension, total_product_price, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orders (id, name, user_id, email, phone, content, total, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: pages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pages (id, author_id, title, excerpt, body, image, slug, meta_description, meta_keywords, status, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: permission_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.permission_role (permission_id, role_id) FROM stdin;
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	1
11	1
12	1
13	1
14	1
15	1
16	1
17	1
18	1
19	1
20	1
21	1
22	1
23	1
24	1
25	1
26	1
27	1
28	1
29	1
30	1
31	1
32	1
33	1
34	1
35	1
36	1
42	1
43	1
44	1
45	1
46	1
47	1
48	1
49	1
50	1
51	1
52	1
53	1
54	1
55	1
56	1
62	1
63	1
64	1
65	1
66	1
67	1
68	1
69	1
70	1
71	1
72	1
73	1
74	1
75	1
76	1
77	1
78	1
79	1
80	1
81	1
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.permissions (id, key, table_name, created_at, updated_at) FROM stdin;
1	browse_hooks	\N	2020-07-14 10:51:13	2020-07-14 10:51:13
2	browse_admin	\N	2020-07-14 11:01:43	2020-07-14 11:01:43
3	browse_bread	\N	2020-07-14 11:01:43	2020-07-14 11:01:43
4	browse_database	\N	2020-07-14 11:01:43	2020-07-14 11:01:43
5	browse_media	\N	2020-07-14 11:01:43	2020-07-14 11:01:43
6	browse_compass	\N	2020-07-14 11:01:43	2020-07-14 11:01:43
7	browse_menus	menus	2020-07-14 11:01:43	2020-07-14 11:01:43
8	read_menus	menus	2020-07-14 11:01:43	2020-07-14 11:01:43
9	edit_menus	menus	2020-07-14 11:01:43	2020-07-14 11:01:43
10	add_menus	menus	2020-07-14 11:01:43	2020-07-14 11:01:43
11	delete_menus	menus	2020-07-14 11:01:43	2020-07-14 11:01:43
12	browse_roles	roles	2020-07-14 11:01:43	2020-07-14 11:01:43
13	read_roles	roles	2020-07-14 11:01:43	2020-07-14 11:01:43
14	edit_roles	roles	2020-07-14 11:01:43	2020-07-14 11:01:43
15	add_roles	roles	2020-07-14 11:01:43	2020-07-14 11:01:43
16	delete_roles	roles	2020-07-14 11:01:43	2020-07-14 11:01:43
17	browse_users	users	2020-07-14 11:01:43	2020-07-14 11:01:43
18	read_users	users	2020-07-14 11:01:43	2020-07-14 11:01:43
19	edit_users	users	2020-07-14 11:01:43	2020-07-14 11:01:43
20	add_users	users	2020-07-14 11:01:43	2020-07-14 11:01:43
21	delete_users	users	2020-07-14 11:01:43	2020-07-14 11:01:43
22	browse_settings	settings	2020-07-14 11:01:43	2020-07-14 11:01:43
23	read_settings	settings	2020-07-14 11:01:43	2020-07-14 11:01:43
24	edit_settings	settings	2020-07-14 11:01:43	2020-07-14 11:01:43
25	add_settings	settings	2020-07-14 11:01:43	2020-07-14 11:01:43
26	delete_settings	settings	2020-07-14 11:01:43	2020-07-14 11:01:43
27	browse_sliders	sliders	2020-07-14 11:22:10	2020-07-14 11:22:10
28	read_sliders	sliders	2020-07-14 11:22:10	2020-07-14 11:22:10
29	edit_sliders	sliders	2020-07-14 11:22:10	2020-07-14 11:22:10
30	add_sliders	sliders	2020-07-14 11:22:10	2020-07-14 11:22:10
31	delete_sliders	sliders	2020-07-14 11:22:10	2020-07-14 11:22:10
32	browse_categories	categories	2020-07-15 03:50:07	2020-07-15 03:50:07
33	read_categories	categories	2020-07-15 03:50:07	2020-07-15 03:50:07
34	edit_categories	categories	2020-07-15 03:50:07	2020-07-15 03:50:07
35	add_categories	categories	2020-07-15 03:50:07	2020-07-15 03:50:07
36	delete_categories	categories	2020-07-15 03:50:07	2020-07-15 03:50:07
42	browse_agronomy_services	agronomy_services	2020-07-15 04:45:51	2020-07-15 04:45:51
43	read_agronomy_services	agronomy_services	2020-07-15 04:45:51	2020-07-15 04:45:51
44	edit_agronomy_services	agronomy_services	2020-07-15 04:45:51	2020-07-15 04:45:51
45	add_agronomy_services	agronomy_services	2020-07-15 04:45:51	2020-07-15 04:45:51
46	delete_agronomy_services	agronomy_services	2020-07-15 04:45:51	2020-07-15 04:45:51
47	browse_news	news	2020-07-15 06:14:23	2020-07-15 06:14:23
48	read_news	news	2020-07-15 06:14:23	2020-07-15 06:14:23
49	edit_news	news	2020-07-15 06:14:23	2020-07-15 06:14:23
50	add_news	news	2020-07-15 06:14:23	2020-07-15 06:14:23
51	delete_news	news	2020-07-15 06:14:23	2020-07-15 06:14:23
52	browse_news_media	news_media	2020-07-15 06:15:31	2020-07-15 06:15:31
53	read_news_media	news_media	2020-07-15 06:15:31	2020-07-15 06:15:31
54	edit_news_media	news_media	2020-07-15 06:15:31	2020-07-15 06:15:31
55	add_news_media	news_media	2020-07-15 06:15:31	2020-07-15 06:15:31
56	delete_news_media	news_media	2020-07-15 06:15:31	2020-07-15 06:15:31
62	browse_product_media	product_media	2020-07-15 09:28:13	2020-07-15 09:28:13
63	read_product_media	product_media	2020-07-15 09:28:13	2020-07-15 09:28:13
64	edit_product_media	product_media	2020-07-15 09:28:13	2020-07-15 09:28:13
65	add_product_media	product_media	2020-07-15 09:28:13	2020-07-15 09:28:13
66	delete_product_media	product_media	2020-07-15 09:28:13	2020-07-15 09:28:13
67	browse_products	products	2020-07-15 09:29:13	2020-07-15 09:29:13
68	read_products	products	2020-07-15 09:29:13	2020-07-15 09:29:13
69	edit_products	products	2020-07-15 09:29:13	2020-07-15 09:29:13
70	add_products	products	2020-07-15 09:29:13	2020-07-15 09:29:13
71	delete_products	products	2020-07-15 09:29:13	2020-07-15 09:29:13
72	browse_featured_products	featured_products	2020-07-15 10:20:35	2020-07-15 10:20:35
73	read_featured_products	featured_products	2020-07-15 10:20:35	2020-07-15 10:20:35
74	edit_featured_products	featured_products	2020-07-15 10:20:35	2020-07-15 10:20:35
75	add_featured_products	featured_products	2020-07-15 10:20:35	2020-07-15 10:20:35
76	delete_featured_products	featured_products	2020-07-15 10:20:35	2020-07-15 10:20:35
77	browse_subcategories	subcategories	2020-07-20 04:20:35	2020-07-20 04:20:35
78	read_subcategories	subcategories	2020-07-20 04:20:35	2020-07-20 04:20:35
79	edit_subcategories	subcategories	2020-07-20 04:20:35	2020-07-20 04:20:35
80	add_subcategories	subcategories	2020-07-20 04:20:35	2020-07-20 04:20:35
81	delete_subcategories	subcategories	2020-07-20 04:20:35	2020-07-20 04:20:35
\.


--
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.posts (id, author_id, category_id, title, seo_title, excerpt, body, image, slug, meta_description, meta_keywords, status, featured, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: product_media; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_media (id, product_id, type, image_path, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.products (id, title, category_id, price, description, keywords, zone_of_maturation, region, female_line, male_line, silk_ugd, black_ugd, agronomic_chars, cob_chars, introduction_guide, manager_id, subcategory_id, dimension, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: questions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.questions (id, question, question_kz, question_ru, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name, display_name, created_at, updated_at) FROM stdin;
1	admin	Administrator	2020-07-14 10:50:28	2020-07-14 10:50:28
2	user	Normal User	2020-07-14 11:01:43	2020-07-14 11:01:43
\.


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.settings (id, key, display_name, value, details, type, "order", "group") FROM stdin;
1	site.title	Site Title	Site Title		text	1	Site
2	site.description	Site Description	Site Description		text	2	Site
3	site.logo	Site Logo			image	3	Site
5	admin.bg_image	Admin Background Image			image	5	Admin
6	admin.title	Admin Title	Voyager		text	1	Admin
7	admin.description	Admin Description	Welcome to Voyager. The Missing Admin for Laravel		text	2	Admin
8	admin.loader	Admin Loader			image	3	Admin
9	admin.icon_image	Admin Icon Image			image	4	Admin
4	site.google_analytics_tracking_id	Google Analytics Tracking ID	\N		text	4	Site
10	admin.google_analytics_client_id	Google Analytics Client ID (used for admin dashboard)	\N		text	1	Admin
11	site.about_us_text	Text About us	<h2 style="box-sizing: border-box; outline: none; margin: 0px 0px 1.5rem; line-height: 1.2; font-size: 25px; padding: 0px; border: 0px; vertical-align: baseline; font-family: AvenirNextCyr; color: #1e2329; background-color: #f9f9f9;">KAZSEEDS is a seed company dedicated to helping our customers attain whole farm profitability and sustainability.</h2>\r\n<p style="box-sizing: border-box; outline: none; margin: 0px 0px 1rem; padding: 0px; border: 0px; font-size: 18px; vertical-align: baseline; font-family: AvenirNextCyr; line-height: 1.2; color: #1e2329; background-color: #f9f9f9;">By understanding your whole operation, we can help you choose the right products and help you manage them on your farm in a way that conserves resources and improves profitability.</p>	\N	rich_text_box	6	Site
13	site.first_advantage	First advantage	Building seed production capabilitiesin Kazakhstan	\N	text	7	Site
14	site.second_advantage	Second advantage	Building seed production capabilitiesin Kazakhstan	\N	text	8	Site
15	site.third_advantage	Third advantage	Building seed production capabilitiesin Kazakhstan	\N	text	9	Site
16	site.fourth_advantage	Fourth advantage	Building seed production capabilitiesin Kazakhstan	\N	text	10	Site
17	site.phone	Phone	+7 (727) 225-61-77	\N	text	11	Site
18	site.address	Address	145 Office, 55/17 Manglik Yel, Yesil District, Nur-Sultan city, Z05T3D6, Republic of Kazakhstan	\N	text	12	Site
25	services-en.first_image	First Image	settings/July2020/bhFRz6nMQvqhnmk1Cav2.png	\N	image	18	Services_en
19	site.email	Email	info@kazseeds.kz	\N	text	13	Site
20	contact-en.employees_text	Employees	<h3 style="box-sizing: border-box; outline: none; margin: 0px 0px 1rem; line-height: 1.2; font-size: 25px; padding: 0px; border: 0px; vertical-align: baseline; font-family: AvenirNextCyr; color: #1e2329; background-color: #f9f9f9;">Expect to see us more than once each year!</h3>\r\n<p style="box-sizing: border-box; outline: none; margin: 0px; padding: 0px; border: 0px; font-size: 18px; vertical-align: baseline; font-family: AvenirNextCyr; line-height: 1.722; color: #1e2329; background-color: #f9f9f9;">We are here to do more than sell you seed. We want your operation to be profitable and sustainable. Seed is only a part of this equation. The best way to personalize our recommendations is to understand your operation. We plan to visit your farm a minimum of three times each growing season.</p>	\N	rich_text_box	14	Contact_en
21	contact-en.employees_image	Employees_image	settings/July2020/CzIl4Ceg7P6qMhRnPHeH.png	\N	image	15	Contact_en
22	services-en.service_text	How we work main Text	In addition to seeds, however, good agronomy is paramount to improving overall productivity and the financial aspect of a farming operation. We want to sell you seed, but you will see us more often than just at sales time. We want to visit your farm during each growing season.	\N	text	16	Services_en
24	services-en.additional_text	How we work additional text	<p style="box-sizing: border-box; outline: none; margin: 0px 0px 1.5rem; padding: 0px; border: 0px; font-size: 18px; vertical-align: baseline; font-family: AvenirNextCyr; line-height: 1.722; color: #1e2329;">We want to understand your operation so we can make personalized recommendations for hybrids and varieties and also for other important areas of management: fertility, weed control, planting population, equipment settings. Think of us as your agronomy team. We want you to succeed.</p>\r\n<p style="box-sizing: border-box; outline: none; margin: 0px 0px 1.5rem; padding: 0px; border: 0px; font-size: 18px; vertical-align: baseline; font-family: AvenirNextCyr; line-height: 1.722; color: #1e2329;">A one-size-fits-all management style does not work. Every operation may have one goal&mdash;to be profitable&mdash;but the way of getting there is unique for each farm. A strictly grain operation has different needs than a livestock operation. Poultry farms have different feed requirements than beef operations, which are different from dairy farms. Dryland and irrigated farms are different. Northern farms and southern farms are different. Each farm requires different recommendations, and we will be there to provide them.</p>\r\n<p style="box-sizing: border-box; outline: none; margin: 0px; padding: 0px; border: 0px; font-size: 18px; vertical-align: baseline; font-family: AvenirNextCyr; line-height: 1.722; color: #1e2329;">We are students of history. We learn from watching the past: what has worked and what can lead us down the path to future environmental sustainability. Our goal is to help you realize both short-term profitability and long-term profitability.</p>	\N	rich_text_box	17	Services_en
26	services-en.citation_title	Citation Title	Our Knowledge is Your Knowledge	\N	text	19	Services_en
28	services-en.citation_text	Citation Text	Each year we conduct trials in a variety of farming systems that utilize different agronomic practices: no-till to full tillage, dryland and irrigated, and different crop rotations.  Additionally we screen many non-seed agronomic products each year, keeping us current with available herbicide, fungicide, insecticide, and fertility options. We know that every farm is unique.  We have over 190 years of combined experience in agriculture.  Let us use them on your farm!	\N	text_area	20	Services_en
29	services-en.citation_image	Citation image	settings/July2020/3KRU7y2LevlDNYtWKSFC.png	\N	image	21	Services_en
31	about-en.mission_title	Mission Title	The mission of Kazseeds is to improve farm productivity in Kazakhstan. We envision a Kazakhstan that is self-sufficient in food production and an agricultural exporter to our growing neighbors.	\N	text_area	22	About_en
32	about-en.mission_text	Mission  Text	<p><span style="color: #1e2329; font-family: AvenirNextCyr; font-size: 18px; background-color: #f9f9f9;">We started a seed company because we believe that high quality seeds are one important part to improving productivity. Seed development takes time: it takes generations. Working with a group that values history and time to provide proven genetics is important. We source proven nonGMO genetics from a partner company in the United States with similar values. We then test those genetics for performance in Kazakhstan. In our first year, our products dramatically increased the feed efficiency for a local beef operation. Since then we have expanded to more regions throughout Kazakhstan, and are in the process of increasing our range of products.</span></p>	\N	rich_text_box	23	About_en
\.


--
-- Data for Name: sliders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sliders (id, title, image_path, link, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: subcategories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.subcategories (id, category_id, title, created_at, updated_at) FROM stdin;
1	1	New Book	2020-07-21 05:15:58	2020-07-21 05:15:58
\.


--
-- Data for Name: translations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.translations (id, table_name, column_name, foreign_key, locale, value, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_roles (user_id, role_id) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, email, phone, email_verified_at, password, remember_token, created_at, updated_at, avatar, role_id, settings) FROM stdin;
1	admin	your@email.com	\N	\N	$2y$10$KOILCpxNxa9wieu2E3ijSeA2sVQzAyi0vvFJIYdZbrxgB0/MVB/ni	qduggjmY9VBCZhfx7qAzmPj44ePeVG0HLWZWbf0AgAaxfBqxY1TvNkGs2ee7	2020-07-14 10:50:28	2020-07-14 10:50:28	users/default.png	1	\N
2	asyl	mail.ru	\N	\N	eyJpdiI6IjFZYjNDeFkrUTgxNTV6U3FPKzhaWXc9PSIsInZhbHVlIjoiRkJjZ3ZwVjEzU1o0ZEl1S2lpTlRQdz09IiwibWFjIjoiNGYyYzVjZGU1MDlkM2Q1N2YxYzVlODI3NTU3ZDM3N2NlNzBhZTU2NjNhY2ZlZjY3MmU2NjVhYzBkOWEyZGI4NiJ9	\N	2020-07-21 06:44:48	2020-07-21 06:44:48	users/default.png	1	\N
3	asyl	mail@c.rus	\N	\N	eyJpdiI6ImNUTHdqRG5KQUdYMjVRQ1A0NXpaQ2c9PSIsInZhbHVlIjoiN0R5UGh3bUlEZjQxZEpwcDJtVFhWQT09IiwibWFjIjoiOGE4MjU4MmY2MzUyNWQ2Njk3ZTBiOGMyNjJmYTMzZDRkNmIzM2Y4ODE4MWNlNGQ5ZTIyMzEwNDI4Yzg0NDczZiJ9	\N	2020-07-21 09:24:20	2020-07-21 09:24:20	users/default.png	1	\N
4	asyl	mail@c.russ	\N	\N	eyJpdiI6ImZ6T3B2NDBPR2hTbFE2R0MyL0VTN0E9PSIsInZhbHVlIjoieXg4RzByY25PR3VFalVSNG5wWElodz09IiwibWFjIjoiOWVhM2Y0MWRmY2JjYzBiZmVmM2U2NmI4MDkzNGNmMDRjNTY1MjBlMDAxNzczN2Y5NDMzZjlkM2ViYjJhZjI2NSJ9	\N	2020-07-21 09:25:53	2020-07-21 09:25:53	users/default.png	1	\N
5	asyl	mail@c.russs	8707303991	\N	eyJpdiI6InBIemxkY1RNV3g4eGR3ODdOWHN1M0E9PSIsInZhbHVlIjoiYkNlenhFNTBBMmxVRVk2MmFlV2dxdz09IiwibWFjIjoiYWNlZGM2MzBiNTZjMzNmZTI2M2FjZDM0MjE2M2FiNzE5YmFlZjk4NjU2MWNmZTAyMDhhNDAxZDY0OGIwNDYwZCJ9	\N	2020-07-21 09:27:15	2020-07-21 09:27:15	users/default.png	1	\N
\.


--
-- Name: agronomy_services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.agronomy_services_id_seq', 11, true);


--
-- Name: answers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.answers_id_seq', 1, false);


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categories_id_seq', 2, true);


--
-- Name: data_rows_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.data_rows_id_seq', 160, true);


--
-- Name: data_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.data_types_id_seq', 15, true);


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- Name: featured_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.featured_products_id_seq', 1, true);


--
-- Name: menu_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.menu_items_id_seq', 22, true);


--
-- Name: menus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.menus_id_seq', 1, true);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 7, true);


--
-- Name: news_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.news_id_seq', 2, true);


--
-- Name: news_media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.news_media_id_seq', 2, true);


--
-- Name: order_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.order_products_id_seq', 1, false);


--
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orders_id_seq', 1, false);


--
-- Name: pages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pages_id_seq', 1, false);


--
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.permissions_id_seq', 81, true);


--
-- Name: posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.posts_id_seq', 1, false);


--
-- Name: product_media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_media_id_seq', 5, true);


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.products_id_seq', 4, true);


--
-- Name: questions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.questions_id_seq', 1, false);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 2, true);


--
-- Name: settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.settings_id_seq', 32, true);


--
-- Name: sliders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sliders_id_seq', 5, true);


--
-- Name: subcategories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.subcategories_id_seq', 1, true);


--
-- Name: translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.translations_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 6, false);


--
-- Name: agronomy_services agronomy_services_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agronomy_services
    ADD CONSTRAINT agronomy_services_pkey PRIMARY KEY (id);


--
-- Name: answers answers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answers
    ADD CONSTRAINT answers_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: data_rows data_rows_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_rows
    ADD CONSTRAINT data_rows_pkey PRIMARY KEY (id);


--
-- Name: data_types data_types_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_types
    ADD CONSTRAINT data_types_name_unique UNIQUE (name);


--
-- Name: data_types data_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_types
    ADD CONSTRAINT data_types_pkey PRIMARY KEY (id);


--
-- Name: data_types data_types_slug_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_types
    ADD CONSTRAINT data_types_slug_unique UNIQUE (slug);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: featured_products featured_products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.featured_products
    ADD CONSTRAINT featured_products_pkey PRIMARY KEY (id);


--
-- Name: menu_items menu_items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu_items
    ADD CONSTRAINT menu_items_pkey PRIMARY KEY (id);


--
-- Name: menus menus_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menus
    ADD CONSTRAINT menus_name_unique UNIQUE (name);


--
-- Name: menus menus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menus
    ADD CONSTRAINT menus_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: news_media news_media_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_media
    ADD CONSTRAINT news_media_pkey PRIMARY KEY (id);


--
-- Name: news news_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);


--
-- Name: order_products order_products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_products
    ADD CONSTRAINT order_products_pkey PRIMARY KEY (id);


--
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: pages pages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pages
    ADD CONSTRAINT pages_pkey PRIMARY KEY (id);


--
-- Name: pages pages_slug_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pages
    ADD CONSTRAINT pages_slug_unique UNIQUE (slug);


--
-- Name: permission_role permission_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_pkey PRIMARY KEY (permission_id, role_id);


--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: posts posts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: posts posts_slug_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_slug_unique UNIQUE (slug);


--
-- Name: product_media product_media_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_media
    ADD CONSTRAINT product_media_pkey PRIMARY KEY (id);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: questions questions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questions
    ADD CONSTRAINT questions_pkey PRIMARY KEY (id);


--
-- Name: roles roles_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_name_unique UNIQUE (name);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: settings settings_key_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_key_unique UNIQUE (key);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: sliders sliders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sliders
    ADD CONSTRAINT sliders_pkey PRIMARY KEY (id);


--
-- Name: subcategories subcategories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subcategories
    ADD CONSTRAINT subcategories_pkey PRIMARY KEY (id);


--
-- Name: translations translations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.translations
    ADD CONSTRAINT translations_pkey PRIMARY KEY (id);


--
-- Name: translations translations_table_name_column_name_foreign_key_locale_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.translations
    ADD CONSTRAINT translations_table_name_column_name_foreign_key_locale_unique UNIQUE (table_name, column_name, foreign_key, locale);


--
-- Name: user_roles user_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (user_id, role_id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: permission_role_permission_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX permission_role_permission_id_index ON public.permission_role USING btree (permission_id);


--
-- Name: permission_role_role_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX permission_role_role_id_index ON public.permission_role USING btree (role_id);


--
-- Name: permissions_key_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX permissions_key_index ON public.permissions USING btree (key);


--
-- Name: user_roles_role_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_roles_role_id_index ON public.user_roles USING btree (role_id);


--
-- Name: user_roles_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_roles_user_id_index ON public.user_roles USING btree (user_id);


--
-- Name: answers answers_question_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answers
    ADD CONSTRAINT answers_question_id_foreign FOREIGN KEY (question_id) REFERENCES public.questions(id) ON DELETE CASCADE;


--
-- Name: data_rows data_rows_data_type_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_rows
    ADD CONSTRAINT data_rows_data_type_id_foreign FOREIGN KEY (data_type_id) REFERENCES public.data_types(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: featured_products featured_products_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.featured_products
    ADD CONSTRAINT featured_products_product_id_foreign FOREIGN KEY (product_id) REFERENCES public.products(id) ON DELETE CASCADE;


--
-- Name: menu_items menu_items_menu_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu_items
    ADD CONSTRAINT menu_items_menu_id_foreign FOREIGN KEY (menu_id) REFERENCES public.menus(id) ON DELETE CASCADE;


--
-- Name: news_media news_media_news_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_media
    ADD CONSTRAINT news_media_news_id_foreign FOREIGN KEY (news_id) REFERENCES public.news(id) ON DELETE CASCADE;


--
-- Name: order_products order_products_manager_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_products
    ADD CONSTRAINT order_products_manager_id_foreign FOREIGN KEY (manager_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: order_products order_products_order_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_products
    ADD CONSTRAINT order_products_order_id_foreign FOREIGN KEY (order_id) REFERENCES public.orders(id) ON DELETE CASCADE;


--
-- Name: order_products order_products_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_products
    ADD CONSTRAINT order_products_product_id_foreign FOREIGN KEY (product_id) REFERENCES public.products(id) ON DELETE CASCADE;


--
-- Name: orders orders_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: permission_role permission_role_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: permission_role permission_role_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: product_media product_media_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_media
    ADD CONSTRAINT product_media_product_id_foreign FOREIGN KEY (product_id) REFERENCES public.products(id) ON DELETE CASCADE;


--
-- Name: products products_category_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_category_id_foreign FOREIGN KEY (category_id) REFERENCES public.categories(id) ON DELETE CASCADE;


--
-- Name: products products_manager_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_manager_id_foreign FOREIGN KEY (manager_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: products products_subcategory_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_subcategory_id_foreign FOREIGN KEY (subcategory_id) REFERENCES public.subcategories(id) ON DELETE CASCADE;


--
-- Name: subcategories subcategories_category_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subcategories
    ADD CONSTRAINT subcategories_category_id_foreign FOREIGN KEY (category_id) REFERENCES public.categories(id);


--
-- Name: user_roles user_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: user_roles user_roles_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: users users_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- PostgreSQL database dump complete
--

