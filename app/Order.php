<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $fillable = ['name', 'phone', 'message', 'email', 'total', 'content', 'user_id'];


    public function products()
    {
        return $this->hasMany('App\OrderProduct');
    }
}
