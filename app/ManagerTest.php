<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class ManagerTest extends Model
{
    //
    use Translatable;

    protected $translatable = [
      'quesiton'
    ];
    public function answers()
    {
        return $this->hasMany('App\UserAnswer');
    }
}
