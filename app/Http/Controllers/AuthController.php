<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    //
    public function Login(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required'
        ];
        $messages = [];
        $validator = $this->validator($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $validator->errors();
        } else {



            $user = User::where('email', $request['email'])->first();

            if (!$user) {
                //                $message = 'Введеный вами почта или пароль не действительны';
                $message = 'Wrong password or email';
                //                $data['error']['kz'] = 'Почта неменсе құпия сөз дұрыс емес';
                return Response($message, 400);
            }
            if (!Hash::check($request['password'], $user->password)) {
                //                $data['error']['ru'] = 'Введеный вами почта или пароль не действительны';
                $message = 'Wrong password or email';
                //                $data['error']['kz'] = 'Почта неменсе құпия сөз дұрыс емес';
                return Response($message, 400);
            }

            session()->put('user', $user);
            session()->save();
            $data['user'] = $user;

            return Response($data['user'], 200);
        }
    }

    public function Register(Request $request)
    {
        $rules = [
            'email' => 'required|unique:users|email',
            'phone' => 'required|unique:users',
            'name' => 'required',
            'password' => 'required|min:8',
            'password_repeat' => 'required|min:8',
        ];
        $messages = [];
        $validator = $this->validator($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $validator->errors();
        } else {
            if ($request['password'] != $request['password_repeat']) {
                $data['message'] = 'Пароли не совпадают';
                return Response($data, 400);
            }


            $user = new User;
            $user['name'] = $request['name'];
            $user['email'] = $request['email'];
            $user['phone'] = $request['phone'];
            $user['password'] = Hash::make($request['password']);
            $user['remember_token'] = Str::random(60);
            $user->save();


            return Response($user, 200);
        }
    }

    public function EmailCheck(Request $request)
    {
        if ($request['email']) {
            $user = User::where('email', $request['email'])->first();
            if (!$user) {
                $data['message'] = 'ok';

                return Response($data, 200);
            } else {
                $message = 'This email was already used';
                //                $data['message']['ru'] = 'Эта почта занята';
                //                $data['message']['kz'] = 'Почта қолданылыпжатыр';
                return Response($message, 400);
            }
        }
    }

    public function PhoneCheck(Request $request)
    {
        if ($request['phone']) {
            $user = User::where('phone', $request['phone'])->first();
            if (!$user) {
                $message = 'ok';

                return Response($message, 200);
            } else {
                $message = 'This phone was already used';
                //                $data['message']['ru'] = 'Этот номер телефона занята';
                //                $data['message']['kz'] = 'Телефон қолданылыпжатыр';
                return Response($message, 400);
            }
        }
    }

    public function TokenCheck(Request $request){

        if ($request->header('Authorization')){
            $token = str_replace('Bearer ','',$request->header('Authorization'));
            $user = User::where('remember_token', $token)->first();

            if($user)
                return Response($user,200);
            else
                return Response('error',401);
        }
        else
            return Response('error',401);
    }

    public function forgetPassword(Request $request)
    {
        $valData = $request->validate([
            'email' => 'required'
        ]);

        $user = User::where('email',$valData['email'])->first();

        if (!$user){
            return response(['message' => 'Пользователь не найден'],404);
        }
        try {
            Mail::send('email.forget',['data' => $user],function($m) use ($request){
                $m->from('kazseedsinfo@gmail.com','Обратная связь');

                $m->to($request->email,'Kazseeds site')->subject('Восстановление пароля');
            });

            return response()->json([
                'message' => 'Сообщение успешно отправлено'
            ],200);
        }catch (\Exception $e){
            return response()->json([
                'message' => 'Error '.$e
            ],500);
        }

    }

    public function updatePassword(Request $request)
    {
        $valData = $request->validate([
            'password' => 'required',
            'user_id' => 'required'
        ]);

        $user = User::find($valData['user_id']);
        if (!$user){
            return response(['message' => 'User not found'],404);
        }
        $user['password'] = Hash::make($request->password);
        $user->save();
        return response(['message'=> 'Password updated'],200);
    }
}
