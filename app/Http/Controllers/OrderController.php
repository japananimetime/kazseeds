<?php

namespace App\Http\Controllers;

use App\ManagerTest;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\Question;
use App\User;
use App\UserAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    //
    public function Request(Request $request)
    {
        $data = $request->validate([
           'name' => 'required',
           'phone' => 'required',
           'message' => 'required'
        ]);

        try {
            Mail::send('email.callback',['data' => $data],function($m){
                $m->from('kazseedsinfo@gmail.com','Обратная связь');

                $m->to('info@kazseeds.kz','Kazseeds site')->subject('Вам пришло сообщение с сайта');
            });

            $local = Cookie::get('local');
            if ($local == 'ru')
                return response()->json([
                    'message' => 'Сообщение успешно отправлено'
                ],200);
            if ($local == 'kz')
                return response()->json([
                    'message' => 'Хабар сәтті жіберілді'
                ],200);
            if ($local == 'en')
                return response()->json([
                    'message' => 'Message sent successfully'
                ],200);
        }catch (\Exception $e){
            return response()->json([
               'message' => 'Error '.$e
            ],500);
        }

    }
    public function History(Request $request)
    {

        $token = str_replace('Bearer ', '', $request->header('Authorization'));
        if (!$token)
            return Response('error', 400);

        $user = User::where('remember_token', $token)->first();
        if (!$user)
            return Response('error', 400);


        $main_orders = Order::where('user_id', $user['id'])->get();
        $orders = [];
        foreach ($main_orders as $order) {
            foreach ($order->products as $product) {

                $product_card = Product::where('id', $product->product_id)->first();
                $manager = User::where('id', $product->manager_id)->first();
                $product_card['total_price'] = $product->total_product_price;
                $product_card['main_image'] = $product_card->medias->where('type', 'main_image')->first();
                $product_card['manager_name'] = $manager['name'];
                $product_card['quantity'] = $product->quantity;
                $product_card['status'] = $product->status;
                $product_card['main_image']['image_path'] = json_decode($product_card['main_image']['image_path'])[0];

                if ($product_card['status'] == 'delivered' || $product_card['status'] == 'ordered')
                    $product_card['status_color'] = '#086f35';
                elseif ($product_card['status'] == 'waiting')
                    $product_card['status_color'] = '#f68a2e';
                elseif ($product_card['status'] == 'rejected')
                    $product_card['status_color'] = '#e74c3c';
                $orders[] = $product_card;
            }
        }

        $data['orders'] = [];

        $data['orders'] = $orders;

        return Response($data['orders'], 200);
    }

    public function GetQuestions()
    {
        $questions = ManagerTest::get();
        $lang = Cookie::get('local');
        $randCount = rand(2,5);

        if (count($questions) <= 3)
            $randCount = rand(2,count($questions));
        $questionMain = [];
        for($i = 1; $i<=$randCount;$i++){

            $randMain = rand(1,count($questions));

            $question = ManagerTest::find($randMain);

            if ($question){
                $question = $question->translate($question, $lang);
                $question['answers'] = $question->answers;

                $questionMain[] = $question;
            }


        }
        return response()->json([
           'questions' => $questionMain
        ],200);


    }
    public function TestCompleted(Request $request){
        $validated = $request->validate([
           'answers' => 'required'
        ]);

        foreach ($validated['answers'] as $answer){
            $answer = UserAnswer::find($answer);
            $answer['counts'] += 1;
            $answer->save();
        }
        $local = Cookie::get('local');
        if ($local == 'en')
            return response()->json(['msg' => 'Test completed'],200);
        if ($local == 'ru')
            return response()->json(['msg' => 'Опрос закрыт спасибо'],200);
        if ($local == 'kz')
            return response()->json(['msg' => 'Сауалнама рахметімен жабылдыы'],200);

    }
}
