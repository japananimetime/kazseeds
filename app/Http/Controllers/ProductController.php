<?php

namespace App\Http\Controllers;

use App\Category;
use App\OrderProduct;
use App\Order;
use App\Product;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use TCG\Voyager\Facades\Voyager;


class ProductController extends Controller
{
    //

    public function pagination($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function Index(Request $request)
    {

        $data['products'] = Product::with(['medias','categories'])->join('categories','categories.id','=','products.category_id')->select('products.*','categories.order')->orderBy('categories.order')->orderBy('action','asc')->get();
        $local = Cookie::get('local');
        $data['products'] = collect($data['products']);
        if ($request['category_id'])
            $data['products'] = $data['products']->where('category_id', $request['category_id']);

        if ($request['subcategory_id'])
            $data['products'] = $data['products']->where('subcategory_id', $request['subcategory_id']);

        if ($request['sortRule'] == 'asc') {
            $data['products'] = $data['products']->sortBy('action')->sortBy('categories.order');
        } elseif ($request['sortRule'] == 'desc') {
            $data['products'] = $data['products']->sortByDesc('action')->sortBy('categories.order');
        }
        if ($request['sortBy'])
            $data['products'] = $data['products']->sortBy($request['sortBy']);




        if(!$request->has('count'))
            $request->count = 10;


        if ($request->has('page')){
            for ($i = (($request->page*$request->count)-$request->count); $i<($request->page*$request->count); $i++){
                if (!empty($data['products'][$i])) {
                    $data['products'][$i] = $data['products'][$i]->translate($data['products'][$i], $local);
                }
            }
        }

        if (!$request->has('page')) {

            for ($i = 0; $i < $request->count; $i++) {
                if (!empty($data['products'][$i])) {
                    $data['products'][$i] = $data['products'][$i]->translate($data['products'][$i], $local);
                }
            }
        }


        if ($request['count']) {
            $data['products'] = $this->pagination($data['products'], $request['count']);
        } elseif (!$request['count']) {

            $data['products'] = $this->pagination($data['products'], 10);
        }



        foreach ($data['products'] as $product) {
            $main_image = $product->medias->where('type', 'main_image')->first();

            $main_image = json_decode($main_image['image_path'])[0];
            $product['main_image'] = $main_image;
        }

        $data['categories'] = Category::with('subcategories')->orderBy('order','asc')->get();
        $data['categories'] = $data['categories']->translate($data['categories'],$local);
        foreach ($data['categories'] as $category) {
//            $category->subcategories = $category->subcategories->sortBy('order','asc');
            $category->subcategories = $category->subcategories->translate($category->subcategories,$local);

        }











        return $data;
    }

    public function SearchProduct(Request $request)
    {
        if ($request['search']) {
            $data['products'] = Product::where('keywords', 'LIKE', '%' . $request['search'] . '%')->orWhere('title', 'LIKE', '%' . $request['search'] . '%')->paginate(10);
            return Response($data['products'], 200);
        } else {
            return Response('Not found', 404);
        }
    }



    public function Basket(Request $request)
    {
        if ($request['basket']) {
            $products = [];
            foreach ($request['basket'] as $item) {
                $local = Cookie::get('local');
                $product = Product::find($item['id']);
                $product = $product->translate($product,$local);
                if ($product) {
                    $product['quantity'] = $item['quantity'];
                    $product['main_image'] = $product->medias->where('type', 'main_image')->first();
                    $product['main_image']['image_path'] = json_decode($product['main_image']['image_path'])[0];
                    $products[] = $product;
                }

            }

            return Response($products, 200);
        } else {
            return Response('bad request', 400);
        }
    }

    public function  Order(Request $request)
    {
        $rules = [
            'name' => 'required',
            'basket' => 'required',
            'phone' => 'required',
            'total' => 'required'


        ];
        $messages = [];
        $validator = $this->validator($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return Response($validator->errors(), 400);
        } else {
            if (!$request['message']) {
                $request['message'] = 0;
            }
            $order = Order::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'content' => $request['message'],
                'user_id' => $request->user_id != "null" ? $request->user_id : null,
                'total' => $request['total'],
            ]);




            foreach ($request['basket'] as $item) {
                $item = json_decode($item);

                $product = Product::find($item->id);
                $order_product = OrderProduct::create([
                    'manager_id' => $product['manager_id'],
                    'quantity' => $item->quantity,
                    'dimension' => $product['dimension'],
                    'order_id' => $order->id,

                    'product_id' => $product->id,
                    'total_product_price' => $item->quantity * $product['price'],
                    'status' => 'waiting'
                ]);
            }
            $order_products = OrderProduct::where('order_id',$order->id)->join('products','products.id','=','order_products.product_id')->get();
            Mail::send('email.order',['order_products' => $order_products,'order' => $order],function($m){
                $m->from('kazseedsinfo@gmail.com','Вам пришел заказ');

                $m->to('info@kazseeds.kz','Kazseeds site')->subject('Вам пришел заказ с сайта');
            });
            $local = Cookie::get('local');
            if(!$local){
                $local = 'en';
            }
            if ($local == 'ru')
                return response()->json([
                     'Спасибо за ваш заказ! Мы свяжемся с вами в ближайшее время'
                ],200);
            if ($local == 'kz')
                return response()->json([
                     'Тапсырыс үшін рахмет! Жақында біз сізбен байланысамыз'
                ],200);
            if ($local == 'en')
                return response()->json([
                    'Thank you for your order! We will be contacting you soon'
                ],200);

        }
    }

    public function UserOrder(Request $request)
    {
        if ($request['user_id']) {

            $data['orders'] = Order::where('user_id', $request['user_id'])->get();
            foreach ($data['orders'] as $order) {
                $order->products;
                foreach ($order->products as $product) {
                    $product->items;
                    foreach ($product->items as $item) {

                        $item['main_image'] = $item->medias->where('type', 'main_image')->first();
                    }
                }
            }

            return Response($data, 200);
        }
        return Response('error', 400);
    }
}
