<?php

namespace App\Http\Controllers;

use App\AgronomyService;
use App\FeaturedProduct;
use App\News;
use App\Product;
use App\Slider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;

class FrontController extends Controller
{
    //
    public function Index($local = 'en'){

        $data['local'] = $this->SetLocals($local);
        $cookie = Cookie::queue('local',$local,500);;
        $local = session()->get('local');
        if(!$local){
            session()->put('local',$local);
        }
        session()->save();
        $data['sliders'] = Slider::get();
        $data['sliders'] = $data['sliders']->translate($data['sliders'],$local);
        $data['products'] = Product::orderBy('id', 'desc')->paginate(10);
        $data['products'] = $data['products']->translate($data['products'], $local);
        foreach($data['products'] as $product){
            $main_image = $product->medias->where('type', 'main_image')->first();

            $main_image = json_decode($main_image['image_path']);
            $main_image = $main_image[0];
            $product['main_image'] = $main_image;
        }
        $data['news']  = News::orderBy('id','desc')->limit(10)->get();
        $data['news'] = $data['news']->translate($data['news'],$local);


        return view('home.index',$data);
    }

    public function Questionnaire()
    {
        return view('questionary');
    }
    public function Cart($local = 'en')
    {
        $data['local'] = $this->SetLocals($local);
        $cookie = Cookie::queue('local',$local,500);
        return view('cart.index',['local'=>$local]);
    }
    public function Services($local = 'en'){

        $data['local'] = $this->SetLocals($local);
        $data['active'] = 'services';
        $cookie = Cookie::queue('local',$local,500);



        $data['services'] = AgronomyService::get();
        $data['services'] = $data['services']->translate($data['services'],$local);
        return view('company.services',$data);
    }

    public function Contacts($local = 'en'){

        $data['local'] = $this->SetLocals($local);
        $cookie = Cookie::queue('local',$local,500);


        $data['active'] = 'contacts';
        return view('company.contacts',$data);

    }
    public function Products($local = 'en'){
        $data['local'] = $this->SetLocals($local);

        Cookie::queue('local', $local, 50);

        $data['active'] = 'products';

        return view('products.index', $data);

    }
    public function About($local = 'en'){
        $data['local'] = $this->SetLocals($local);
        $cookie = Cookie::queue('local',$local,500);


        $data['active'] = 'about';
        return view('company.about', $data);
    }
    public function Product($id,$local = 'en'){
        $data['local'] = $this->SetLocals($local);
        $cookie = Cookie::queue('local',$local,500);


        $data['product'] = Product::find($id);
        $data['product'] = $data['product']->translate($data['product'],$local);
        $data['images'] = [];
        if ($data['product']) {
            $images = Product::find($id)->medias()->where('type', 'other_images')->get();
            foreach ($images as $image){
                $image_array = json_decode($image['image_path']);
                foreach ($image_array as $item){
                    $data['images'][] = $item;
                }
            }

        }


        $data['features'] = FeaturedProduct::where('product_id','!=',$id)->get();
        foreach ($data['features'] as $feature){
            $feature->product = $feature->product->translate($data['product'],$local);
            $feature['main_image'] = json_decode($feature->product->medias->where('type','main_image')[0]->image_path)[0];
        }



        return view('products.show',$data);
    }
    public function Personal($local = 'en'){

        $data['local'] = $this->SetLocals($local);
        $cookie = Cookie::queue('local',$local,500);


        return view('personal.index',$data);
    }
    public function News($local = 'en'){
        $data['local'] = $this->SetLocals($local);
        $cookie = Cookie::queue('local',$local,500);


        $data['active'] = 'news';
        $data['news'] = News::orderBy('id','desc')->paginate(3);





        return view('company.news',$data);
    }

    public function Forget($email, $hashed, $local = 'en')
    {
        $data['local'] = $this->SetLocals($local);
        $user = User::where('email',$email)->first();
        if (!$user){
            abort(404);
        }

        return view('forget',['data'=> $user,'local'=>$data['local']]);


    }
    protected  function SetLocals($local){

        if (!in_array($local, ['en','ru','kz'])){
            $local = 'en';
        }

        session()->put('local',$local);
        session()->save();

        App::setLocale($local);

        return $local;
    }
}
