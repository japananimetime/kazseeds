<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    //
    protected $fillable = ['order_id', 'manager_id', 'product_id', 'quantity', 'dimension', 'total_product_price', 'status'];
    public function items()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }
}
