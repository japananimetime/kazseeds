<?php

namespace App;
use TCG\Voyager\Traits\Translatable;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    use Translatable;
    protected  $translatable = [
        'title',
        'description',
        'zone_of_maturation',
        'agronomic_chars',
        'cob_chars',
        'introduction_guide',
        'keywords',
        'region',
        'action'
    ];
    public function medias(){
        return $this->hasMany('App\ProductMedia');
    }

    public function categories()
    {
        return $this->belongsTo('App\Category');
    }
}
