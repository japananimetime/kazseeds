<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class UserAnswer extends Model
{
    //

    use Translatable;

    protected $translatable = [
        'answer'
    ];
}
