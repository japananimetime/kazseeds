<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Category extends Model
{

    use Translatable;
    protected $translatable = [
        'title'
    ];


    public function subcategories()
    {

        return $this->hasMany('App\Subcategory')->orderBy('order','asc');
    }
}
