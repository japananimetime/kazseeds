<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Subcategory extends Model
{
    //
    use Translatable;
    protected $translatable = [
      'title'
    ];
    public function Products(){
        return $this->hasMany('App\Products');
    }
}
