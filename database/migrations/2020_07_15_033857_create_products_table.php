<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->bigInteger('category_id')->unsigned();
            $table->bigInteger('price');
            $table->text('description');

            $table->text('keywords')->nullable();
            $table->string('zone_of_maturation')->nullable();
            $table->string('region')->nullable();

            $table->string('female_line')->nullable();
            $table->string('male_line')->nullable();
            $table->string('silk_ugd')->nullable();
            $table->string('black_ugd')->nullable();
            $table->text('agronomic_chars')->nullable();
            $table->text('cob_chars')->nullable();
            $table->text('introduction_guide')->nullable();
            $table->bigInteger('manager_id')->unsigned();
            $table->bigInteger('subcategory_id')->unsigned();
            $table->enum('dimension',['kg','tonne','centner','grams'])->default('kg');
            $table->timestamps();
            $table->foreign('subcategory_id')->on('subcategories')->references('id')->onDelete('CASCADE');
            $table->foreign('manager_id')->on('users')->references('id')->onDelete('CASCADE');
            $table->foreign('category_id')->on('categories')->references('id')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
