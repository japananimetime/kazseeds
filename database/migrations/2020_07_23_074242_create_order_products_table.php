<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_id')->unsigned();
            $table->bigInteger('manager_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->integer('quantity');
            $table->string('dimension');
            $table->enum('status',['waiting','ordered','rejected','delivered'])->default('waiting');
            $table->bigInteger('total_product_price');
            $table->foreign('order_id')->on('orders')->references('id')->onDelete('CASCADE');
            $table->foreign('manager_id')->on('users')->references('id')->onDelete('CASCADE');
            $table->foreign('product_id')->on('products')->references('id')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
