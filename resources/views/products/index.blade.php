@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/catalog.css') }}">
@endpush

@section('content')
    <div id="catalog">
        <section class="banner" style="background-image: url('{{'/storage/'.setting('banner-en.banner_catalog')}}')">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <h1 class="title">@lang('site.catalog')</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="products spaced-bottom pt-4">
            <catalog-component  :cataloglocal="`{{ json_encode(__('catalog.product')) }}`"  :local="`{{\Illuminate\Support\Facades\App::getLocale()}}`" :warning="`@lang('product.warning')`" :success="`@lang('product.success')`"></catalog-component>
        </section>
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/catalog.js') }}"></script>
@endpush
