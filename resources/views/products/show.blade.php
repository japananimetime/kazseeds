@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/card.css') }}">
@endpush

@section('content')
    <style>
        #card .description section.products .products-slider .product-card .product-header .tag{
            font-size: 13px !important;
        }
    </style>
    <div id="card">
        <section class="description background-kazseeds py-4" style="background-position: 50% 100%; backgrond-size: auto; background-repeat: no-repeat;">
            <div class="container">
                <div class="row mb-4">
                    <div class="col-md-12 back_a_wrap">
                        <a class="back_a" href="{{ url()->previous() }}">@lang('product.back')</a>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-5 col-sm-12 col-12 mb-4 mb-md-0">
                        <div class="slider-for popup-gallery">
                            @foreach($images as $image)
                            <div>
                            <a class="" href="{{ asset('/storage/'.$image)}}">
                                <img data-lazy="{{ asset('/storage/'.$image) }}" alt="">
                            </a>
                            </div>

                            @endforeach
                        </div>
                        <div class="slider-nav">
                            @foreach($images as $image)
                                <div>
                                    <img data-lazy="{{ asset('/storage/'.$image) }}" alt="">
                                </div>

                            @endforeach
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-7 col-sm-12 col-12">
                        <h1 class="title">{{$product['title']}}</h1>
                        <p>{!! $product['description'] !!}</p>
                        <div class="d-flex align-items-center">
                            <h5>@lang('product.quantity'):</h5>
                            <div class="quantity-controls">
                                <button type="button" class="decrease">-</button>
                                <label>
                                    <input type="number" value="1" disabled>
                                    @if(\Illuminate\Support\Facades\App::getLocale() == 'ru' )
                                        п.е.
                                        @elseif(\Illuminate\Support\Facades\App::getLocale() == 'en')
                                        {{ $product['dimension'] }}
                                        @else
                                        е.б.
                                    @endif
                                </label>
                                <button type="button" class="increase">+</button>
                            </div>
                        <button type="button" class="cart-btn" product-id="{{ $product['id'] }}" data-warning="@lang('product.warning')" data-success="@lang('product.success')">@lang('product.buy')</button>
                        </div>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-xl-12">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" id="pills-1-tab" data-toggle="pill" href="#pills-1" role="tab" aria-controls="pills-1" aria-selected="true">@lang('product.genetics')</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="pills-2-tab" data-toggle="pill" href="#pills-2" role="tab" aria-controls="pills-2" aria-selected="false">@lang('product.agronomic')</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="pills-3-tab" data-toggle="pill" href="#pills-3" role="tab" aria-controls="pills-3" aria-selected="false">@lang('product.cob')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-4-tab" data-toggle="pill" href="#pills-4" role="tab" aria-controls="pills-4" aria-selected="false">@lang('product.introduction')</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10 col-12">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-1" role="tabpanel" aria-labelledby="pills-1-tab">
                                {!! $product['zone_of_maturation'] ?? '...' !!}
                            </div>
                            <div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-2-tab">{!! $product['agronomic_chars'] ?? '...'!!}</div>
                            <div class="tab-pane fade" id="pills-3" role="tabpanel" aria-labelledby="pills-3-tab">{!! $product['cob_chars'] ?? '...'!!}</div>
                            <div class="tab-pane fade" id="pills-4" role="tabpanel" aria-labelledby="pills-4-tab">{!! $product['introduction_guide'] ?? '...'!!}</div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="products">
                <div class="container mb-4">
                    <div class="row align-items-center">
                        <div class="col-xl-4 col-lg-4 col-md-3 col-sm-4 col-6">
                            <h1 class="title">@lang('product.featured')</h1>
                        </div>
                        <div class="offset-xl-6 offset-lg-5 offset-md-6 offset-sm-4"></div>
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6">
                            <div class="arrows">
                                <button type="button" class="prev slick-arrow"></button>
                                <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                <button type="button" class="next slick-arrow"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="products-slider">

                    @foreach($features as $feature)
                        <div class="product-card">
                            <div class="product-header">
                                <a href="{{route('Product',$feature->product->id)}}/{{\Illuminate\Support\Facades\App::getLocale()}}">
                                        <img data-lazy="{{ asset('/storage/'.$feature['main_image']    ) }}" alt="Corn">
                                        @if($product['action'] != null)
                                            <span class="tag">{{$feature->product != null ? $feature->product['action'] : null}}</span>
                                        @endif
                                </a>
                            </div>
                            <div class="product-body">
                                <h2 class="name">{{$feature->product->title}}</h2>
                                <div class="d-flex justify-content-between align-items-stretch">
                                    <button class="cart-btn">
                                        <object data="{{ asset('icons/basket.svg')}}" type="image/svg+xml">Cart icon</object>
                                    </button>
                                    <a href="#" class="more">@lang('catalog.product.more')</a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </section>
        </section>
    </div>

@endsection

@push('scripts')
<script src="{{ mix('js/personalLink.js') }}"></script>
<script src="{{ mix('js/card.js') }}"></script>
@endpush
