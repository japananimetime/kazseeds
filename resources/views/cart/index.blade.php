@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/cart.css') }}">
@endpush

@section('content')
    <div id="cart">
        <cart-component :cart="{{json_encode(__('menu'))}}"></cart-component>
        <!-- <script>console.log('@lang('menu.cart')') </script> -->
        @include('partials.contacts')
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/cart.js') }}"></script>
@endpush
