@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/home.css') }}">
@endpush

@section('content')
    <div id="home" class="background-kazseeds" style="background-position: 50% 100%;">
        <section>
            <div class="main-slider">
                @if(count($sliders) == 0)
                <div style="background-image: url({{ asset('images/main-slider_image-01.png') }})">
                    <div class="container">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-xl-8 d-flex flex-column flex-nowrap align-items-center">
									 <h1>KAZSEEDS </h1>
                                <h1>@lang('menu.main_banner') </h1>
                                <a href="{{route('About',\Illuminate\Support\Facades\App::getLocale())}}">@lang('site.more')</a>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                @foreach($sliders as $slider)
                    <div style="background-image: url({{ asset('storage/'.$slider['image_path']) }})">
                        <div class="container">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-xl-8 d-flex flex-column flex-nowrap align-items-center">
                                    <h1>{{$slider['title']}}</h1>
                                    <a href="{{route('About',\Illuminate\Support\Facades\App::getLocale())}}}">@lang('site.more')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                @endif
            </div>
        </section>

        <section class="background-kazseeds" style="background-position: 50% 100%;">
            <section class="about">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-12 mb-4" id="about_us">
                            <h1 class="title">@lang('site.about')</h1>
                        </div>
                        <div class="col-xl-9 col-lg-8 col-12">
                            {!! setting('site-'.$local.'.about_us_text') !!}
                        </div>
                        <div class="col-xl-3 col-lg-4 col-6">

                            <test-component :lang="`{{$local}}`"></test-component>
                        </div>
                    </div>
                </div>
            </section>
            <section class="advantages">
                <div class="container">
                    <div class="row pb-5 mb-lg-5 justify-content-center">
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-8">
                            <h1 class="title">@lang('site.advantages')</h1>
                        </div>
                    </div>
                    <div class="row justify-content-center justify-content-sm-start pt-sm-5" style="min-height: 325px;">
                        <div class="col-xl-3 col-lg-3 col-md-5 col-sm-6 col-9 d-flex my-5 my-lg-0">
                            <div class="advantage-item" data-aos="flip-right" data-aos-duration="1000">
                                <div class="cut">
                                    <a href="#">
                                        <object data="{{ asset('icons/advantage-1.svg')}}" type="image/svg+xml">Advantage icon</object>
                                    </a>
                                </div>
                                <h2>{!! setting('site-'.$local.'.first_advantage') !!}</h2>
                            </div>
                        </div>
                        <div class="offset-md-2 offset-lg-0"></div>
                        <div class="col-xl-3 col-lg-3 col-md-5 col-sm-6 col-9 d-flex my-5 my-lg-0">
                            <div class="advantage-item even" data-aos="flip-left" data-aos-delay="500" data-aos-duration="1000">
                                <div class="cut">
                                    <a href="#">
                                        <object data="{{ asset('icons/advantage-2.svg')}}" type="image/svg+xml">Advantage icon</object>
                                    </a>
                                </div>
                                <h2>{!! setting('site-'.$local.'.second_advantage') !!}</h2>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-5 col-sm-6 col-9 d-flex my-5 my-lg-0">
                            <div class="advantage-item" data-aos="flip-right" data-aos-delay="1000" data-aos-duration="1000">
                                <div class="cut">
                                    <a href="#">
                                        <object data="{{ asset('icons/advantage-3.svg')}}" type="image/svg+xml">Advantage icon</object>
                                    </a>
                                </div>
                                <h2>{!! setting('site-'.$local.'.third_advantage') !!}</h2>
                            </div>
                        </div>
                        <div class="offset-md-2 offset-lg-0"></div>
                        <div class="col-xl-3 col-lg-3 col-md-5 col-sm-6 col-9 d-flex my-5 my-lg-0">
                            <div class="advantage-item even" data-aos="flip-left" data-aos-delay="1500" data-aos-duration="1000">
                                <div class="cut">
                                    <a href="#">
                                        <object data="{{ asset('icons/advantage-4.svg')}}" type="image/svg+xml">Advantage icon</object>
                                    </a>
                                </div>
                                <h2>{!! setting('site-'.$local.'.fourth_advantage') !!}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <section class="products">
            <div class="container mb-4">
                <div class="row align-items-center">
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6">
                        <h1 class="title">@lang('site.product')</h1>
                    </div>
                    <div class="offset-xl-8 offset-lg-8 offset-md-6 offset-sm-4"></div>
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6">
                        <div class="arrows">
                            <button type="button" class="prev slick-arrow"></button>
                            <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                            <button type="button" class="next slick-arrow"></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="products-slider">
                @foreach($products as $product)
                <div class="product-card">
                    <div class="product-header">
                        <a href="/product/{{$product->id}}/{{\Illuminate\Support\Facades\App::getLocale()}}">
                            <img data-lazy="{{ asset('/storage/'.$product['main_image']) }}" alt="Corn">
                            @if($product['action'] != null)
                            <span class="tag">{{$product['action'] != null ? $product['action'] : null}}</span>
                                @endif
                        </a>
                    </div>
                    <div class="product-body">
                        <h2 class="name">{{$product->title}}</h2>
                        <div class="d-flex justify-content-between align-items-stretch">
                            <button class="cart-btn">
                                <object data="{{ asset('icons/basket.svg')}}" type="image/svg+xml">Cart icon</object>
                            </button>
                            <a href="/product/{{$product->id}}/{{\Illuminate\Support\Facades\App::getLocale()}}" class="more">@lang('catalog.product.more')</a>
                        </div>
                    </div>
                </div>
                @endforeach


            </div>
        </section>
        <section class="news">
            <div class="container">
                <div class="row mb-4 align-items-center">
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-6">
                        <h1 class="title">@lang('menu.news')</h1>
                    </div>
                    <div class="offset-xl-8 offset-lg-8 offset-md-7 offset-sm-5"></div>
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6">
                        <div class="arrows">
                            <button type="button" class="prev slick-arrow"></button>
                            <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                            <button type="button" class="next slick-arrow"></button>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center justify-content-sm-start">
                    <div class="col-xl-12 col-sm-12 col-10">
                        <div class="news-slider">
                            @foreach($news as $new)
                                <div class="news-card">
                                <div class="news-header">
                                    <a href="#">
                                        @if(count($new->medias) != 0)
                                            <img src="{{ asset('/storage/'.$new->medias[0]->media_path) }}" alt="">
                                        @endif
                                    </a>
                                </div>
                                <div class="news-body">
                                    <span class="date">{{$new->created_at}}</span>
                                    <h2 class="name">{{$new->title}}</h2>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </section>
        @include('partials.contacts')
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/personalLink.js') }}"></script>
<script src="{{ mix('js/home.js') }}"></script>
@endpush
