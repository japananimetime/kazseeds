@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/news.css') }}">
@endpush

@section('content')
    <div id="news" class="background-kazseeds" style="background-position: 50% 100%;">
        <section class="banner" style="background-image: url('{{'/storage/'.setting('banner-en.banner_news')}}')">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <h1 class="title">@lang('menu.news')</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="news">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        @foreach($news as $item)
                        <div class="news-card">
                            <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12 py-4 py-lg-0 px-lg-0">

                                @if(count($item->medias) != 0)
                                <img src="{{ asset('/storage/'.$item->medias[0]->media_path) }}" alt="">
                                    @endif
                            </div>
                            <div class="col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12 py-4">
                                <p class="date">{{$item->created_at}}</p>
                                <h2 class="title">{{$item->getTranslatedAttribute('title',\Illuminate\Support\Facades\App::getLocale())}}</h2>
                                <p class="content">{!!$item->getTranslatedAttribute('text',\Illuminate\Support\Facades\App::getLocale())!!}</p>
                            </div>
                        </div>
                        @endforeach
                        <div>
                            {{$news->links()}}
                        </div>

                    </div>
                </div>
            </div>
        </section>
        @include('partials.contacts')
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/personalLink.js') }}"></script>
<script src="{{ mix('js/news.js') }}"></script>
@endpush
