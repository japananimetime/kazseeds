@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/about.css') }}">
@endpush

@section('content')
    <div id="about" class="background-kazseeds" style="background-position: 50% 100%; ">
        <section class="banner" style="background-image: url('{{'/storage/'.setting('banner-en.banner_about') }}')">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <h1 class="title">KAZSEEDS</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="mission">
            <div class="container">
                <div class="row mb-4">
                    <div class="col-xl-6 col-lg-6 col-md-7 col-sm-12 col-12 mb-4 mb-md-0">
                        <h1 class="title">@lang('site.mission')</h1>
                        <h3>{!!  setting('about-'.$local.'.mission_title') !!}</h3>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-5 col-sm-12 col-12">
                        <img src="{{ asset('images/about_image-01.png') }}" alt="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <p>{!! setting('about-'.$local.'.mission_text') !!}</p>
                    </div>
                </div>
            </div>
        </section>
        @include('partials.contacts')
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/personalLink.js') }}"></script>
<script src="{{ mix('js/about.js') }}"></script>
@endpush
