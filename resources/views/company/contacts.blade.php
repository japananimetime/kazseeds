@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/contacts.css') }}">
@endpush

@section('content')
    <div id="contacts" class="background-kazseeds" style="background-position: 50% 50%;">
        <section class="banner" style="background-image: url('{{'/storage/'.setting('banner-en.banner_contacts')}}')">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <h1 class="title">@lang('menu.contact')</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="employees">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-5 col-sm-12 col-12">
                        <img src="{{ asset('/storage/'.setting('contact-en.employees_image')) }}" alt="">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-7 col-sm-12 col-12">
                        {!! setting('contact-'.$local.'.employees_text') !!}
                    </div>
                </div>
            </div>
        </section>
        <section id="feedback-section" class="contacts">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-5 col-sm-12 col-12">
                        <h1 class="title">@lang('site.contact')</h1>
                        <div class="contact-item" data-aos="fade-right" data-aos-duration="1000">
                            <div class="icon">
                                <object data="{{ asset('icons/phone.svg')}}" type="image/svg+xml">Phone icon</object>
                            </div>
                            <div>
                                <h2>@lang('site.contact'):</h2>
                                <a href="tel: {{ setting('site-'.$local.'.phone') }}">{{ setting('site-'.$local.'.phone') }}</a>
                            </div>
                        </div>
                        <div class="contact-item" data-aos="fade-right" data-aos-duration="1000">
                            <div class="icon">
                                <object data="{{ asset('icons/address.svg')}}" type="image/svg+xml">Address icon</object>
                            </div>
                            <div>
                                <h2>@lang('site.address'):</h2>
                                <p>{{ setting('site-'.$local.'.address') }}</p>
                            </div>
                        </div>
                        <div class="contact-item" data-aos="fade-right" data-aos-duration="1000">
                            <div class="icon">
                                <object data="{{ asset('icons/mail.svg')}}" type="image/svg+xml">Mail icon</object>
                            </div>
                            <div>
                                <h2>@lang('site.email'):</h2>
                                <a href="mailto: {{ setting('site-'.$local.'.email') }}">{!! setting('site-'.$local.'.email') !!}</a>
                            </div>
                        </div>
                        <div class="contact-item" data-aos="fade-right" data-aos-duration="1000">
                            <a href="{{setting('site.instagram_link')}}">

                                <div class="icon">
                                        <object data="{{ asset('icons/instagram.svg')}}" type="image/svg+xml">{!! setting('site-'.$local.'.instagram') !!}</object>

                                </div>
                            </a>

                            <div>
                                <h2>Instagram:</h2>
                                <p>{{ setting('site-'.$local.'.instagram') }}</p>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="langitude" id="langitude" value="{{setting('site.langitude')}}" >
                    <input type="hidden" name="latitude" id="latitude" value="{{setting('site.latitude')}}">
                    <div class="col-xl-6 col-lg-6 col-md-7 col-sm-12 col-12">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="feedback">
            <div class="container">
                <div class="row justify-content-center mb-5">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5 col-8">
                        <h1 class="title">@lang('site.write')</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="offset-xl-2 offset-lg-2"></div>
                    <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                        <form id="feedbackForm" class="row">
                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <input type="text" name="name" class="form-control" placeholder="@lang('menu.name_placeholder')">
                            </div>
                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <input type="tel" name="phone" class="form-control" placeholder="@lang('menu.phone_placeholder')">
                            </div>
                            <div class="form-group col-xl-12 mt-md-4">
                                <textarea name="message" class="form-control" placeholder="@lang('menu.message_placeholder')"></textarea>
                            </div>
                            <div class="form-group col-xl-12 d-flex justify-content-center mt-4">
                                <button type="submit" class="request-btn">@lang('site.write')</button>
                            </div>
                        </form>
                    </div>
                    <div class="offset-xl-2 offset-lg-2"></div>
                </div>
            </div>
        </section>
    </div>
    @endsection

@push('scripts')
<script src="{{ mix('js/personalLink.js') }}"></script>
<script src="{{ mix('js/contacts.js') }}"></script>
@endpush
