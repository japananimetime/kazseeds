@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/services.css') }}">
@endpush

@section('content')
    <div id="services" class="background-kazseeds" style="background-position: 50% 50%;">
        <section class="banner" style="background-image: url({{'/storage/'.setting('banner-en.banner_services')}})">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <h1 class="title">@lang('menu.agronomy_service')</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container">
                <div class="row mb-4">
                    <div class="col-xl-6 col-lg-6 col-md-7 col-sm-12 col-12 mb-4 mb-md-0">
                        <h1 class="title">@lang('site.we_work')</h1>
                        <h3>{!! setting('services-'.$local.'.service_text')!!}</h3>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-5 col-sm-12 col-12">
                        <img src="{{ asset('/storage/'.setting('services-en.first_image'))}}" height="310" alt="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        {!! setting('services-'.$local.'.additional_text') !!}
                    </div>
                </div>
            </div>
        </section>
        <section class="services">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <h1 class="title">@lang('menu.agronomy_service')</h1>
                    </div>
                    <div class="col-xl-12">
                        <ul>
                            @foreach($services as $service)
                                <li data-aos="zoom-in" data-aos-duration="1000">
                                    <i class="fas fa-square"></i>
                                    <p>{{$service['title']}}</p>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container">
                <div class="row">
                    <div class="col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12 mb-4 mb-md-0">
                        <h2 class="title">{!!  setting('services-'.$local.'.citation_title')  !!}</h2>
                        <p>{!!setting('services-'.$local.'.citation_text')!!}</p>
                    </div>
                    <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                        <img src="{{ asset('/storage/'.setting('services-en.citation_image')) }}" height="385" alt="">
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/personalLink.js') }}"></script>
<script src="{{ mix('js/services.js') }}"></script>
@endpush
