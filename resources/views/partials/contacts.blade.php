<section class="contacts">
    <div class="container">
        <div class="row pb-5 mb-lg-5 justify-content-center">
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6">
                <h1 class="title">@lang('site.contact')</h1>
            </div>
        </div>
        <div class="row justify-content-center mb-5">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                <div class="contact-item" data-aos="fade-right" data-aos-duration="1000">
                    <div class="icon">
                        <object data="{{ asset('icons/phone.svg')}}" type="image/svg+xml">Phone icon</object>
                    </div>
                    <h2>@lang('site.phone'):</h2>
                    <a href="tel: {{ setting('site-'.$local.'.phone') }}">{!! setting('site-'.$local.'.phone') !!}</a>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12 order-1 order-md-0 mt-4 mt-md-0">
                <div class="contact-item" data-aos="fade-up" data-aos-duration="1000">
                    <div class="icon">
                        <object data="{{ asset('icons/address.svg')}}" type="image/svg+xml">Address icon</object>
                    </div>
                    <h2>@lang('site.address'):</h2>
                    <p>{!! setting('site-'.$local.'.address') !!}</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12 order-1 order-md-0 mt-4 mt-md-0">
                <div class="contact-item" data-aos="fade-up" data-aos-duration="1000">
                    <a href="{{setting('site.instagram_link')}}">
                        <div class="icon">

                                <object data="{{ asset('icons/instagram.svg')}}" type="image/svg+xml">Instagram icon</object>
                        </div>
                    </a>

                    <h2>Instagram:</h2>
                    <p>{!! setting('site-'.$local.'.instagram') !!}</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                <div class="contact-item" data-aos="fade-left" data-aos-duration="1000">
                    <div class="icon">
                        <object data="{{ asset('icons/mail.svg')}}" type="image/svg+xml">Mail icon</object>
                    </div>
                    <h2>@lang('site.email'):</h2>
                    <a href="mailto: {{setting('site-'.$local.'.email')}}">{!! setting('site-'.$local.'.email') !!}</a>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-8">
                <a class="contact-btn" role="button" href="{{route('Contacts').'/'.\Illuminate\Support\Facades\App::getLocale()}}">@lang('site.contact_us')</a>
            </div>
        </div>
    </div>
</section>
