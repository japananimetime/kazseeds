<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="msapplication-TileColor" content="#086f35">
    <meta name="theme-color" content="#086f35">
    <link rel="mask-icon" href="{{ asset('icons/favicon.svg') }}">
    <link rel="icon" href="{{ asset('icons/favicon.svg') }}" sizes="any" type="image/svg+xml">
    <title>KAZSEEDS</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    @stack('styles')
</head>
<body>
    <div id="app">
        <div id="header">
            <section class="background-limegreen">
                <div class="container">
                    <div class="row align-items-center py-sm-2 py-lg-0">
                        <div class="col-xl-5 col-lg-5 col-md-1 col-sm-1 col-2 align-self-stretch">
                            <a class="toggle" href="#">
                                <span></span>
                            </a>
                            <nav class="main-nav @if(\Illuminate\Support\Facades\App::getLocale() == 'kz')kazakh @endif" role="navigation">
                                <ul>
                                    <li class=" @if(isset($active) && $active == 'about' )active @endif ">

                                        <a href="{{route('About',$local)}}">@lang('menu.mission')</a>
                                    </li>
                                    <li class="@if(isset($active) && $active == 'products')active @endif ">
                                        <a href="{{route('Products',$local)}}">@lang('menu.products')</a>
                                    </li>
                                    <li class="@if(isset($active) && $active == 'news')active @endif ">
                                        <a href="{{route('News',$local)}}">@lang('menu.news')</a>
                                    </li>
                                    <li class="@if(isset($active) && $active == 'services' )active @endif ">
                                        <a href="{{route('Services',$local)}}">@lang('menu.agronomy_service')</a>
                                    </li>
                                    <li class="@if(isset($active) && $active == 'contacts')active @endif ">
                                        <a href="{{route('Contacts',$local)}}">@lang('menu.contact')</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="offset-2 offset-sm-0"></div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-4 d-flex d-sm-block justify-content-end">
                            <a href="{{\Illuminate\Support\Facades\Route::currentRouteName() === 'Index' ? '#about_us' : route('Index', $local) }}" class="logo">
                                <object data="{{ asset('icons/logo.svg')}}" type="image/svg+xml">Logo icon</object>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-8 my-3 my-sm-0 order-5 order-sm-0 px-0">
                            <nav class="minor-nav" role="navigation">
                                <ul>
                                    <!-- <li>
                                        <a class="d-flex align-items-center" href="{{route('Contacts', $local)}}#feedback-section">
                                            <object data="{{ asset('icons/phone.svg')}}" type="image/svg+xml">Phone icon</object>
                                            @lang('menu.feed_back')
                                        </a>
                                    </li> -->
                                    <li>
                                        <personal-link :auth-text="`@lang('auth.auth')`" :auth="`{{json_encode(__('auth'))}}`" :link_title="`@lang('menu.personal')`" :link_type="`{{route('Personal', $local)}}`"  :email="`{{ json_encode(__('validation.custom.email')) }}`" :name="`{{ json_encode(__('validation.custom.name')) }}`" :phone="`{{ json_encode(__('validation.custom.phone')) }}`" :password="`{{ json_encode(__('validation.custom.password')) }}`"></personal-link>
                                    </li>
                                    <li>
                                        <a class="d-flex align-items-center" href="{{route('Cart',$local)}}">
                                            <object data="{{ asset('icons/basket.svg')}}" type="image/svg+xml">Cart icon</object>
                                        </a>
                                    </li>
                                    <li class="icons">
                                        <a href="{{setting('site.instagram_link')}}"  style="font-size: 18px;margin-right: 5px;">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li class="icons" >
                                        <a href="{{setting('site.facebook_link')}}" style="font-size: 18px;margin-right: 5px;">
                                            <i class="fab fa-facebook "></i>
                                        </a>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-4 pd-0-pr-8 d-flex d-md-block justify-content-end">
								<a href="{{route('questionnaire')}}" class="download-link" target="_blank"> @lang('menu.testimonial')</a>
                    </div>
						  <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-3 px-0">
								<nav class="minor-nav mobil-pos" role="navigation">
                                    <ul>
										  <li>
                                        @php $lang= app()->getLocale()                                       ; @endphp
                                        <ol class="lang">
                                            @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'Product')
                                            <li class="@if($lang == 'ru') active @endif">
                                                <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(),[$product['id'],'ru'])}}">Ru</a>
                                            </li>
                                            <li class="@if($lang == 'kz') active @endif">
                                                <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(),[$product['id'],'kz'])}}">Kz</a>
                                            </li>
                                            <li class="@if($lang == 'en') active @endif">
                                                <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(),[$product['id'],'en'])}}">En</a>
                                            </li>
                                            @else
                                                <li class="@if($lang == 'ru') active @endif">
                                                    <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(),'ru')}}">Ru</a>
                                                </li>
                                                <li class="@if($lang == 'kz') active @endif">
                                                    <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(),'kz')}}">Kz</a>
                                                </li>
                                                <li class="@if($lang == 'en') active @endif">
                                                    <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(),'en')}}">En</a>
                                                </li>
                                            @endif
                                        </ol>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                </div>
            </section>
        </div>
        @yield('content')
        <div id="footer">
            <section>
                <div class="container">
                    <div class="row align-items-center py-4">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-5 col-4">
                            <p>KAZSEEDS &copy; 2020 @lang('menu.rights')</p>
                        </div>
                        <div class="offset-xl-2 offset-lg-2 offset-md-2"></div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-4 d-flex justify-content-center">
                            <a href="/" class="logo">
                                <object data="{{ asset('icons/footer-logo.svg')}}" type="image/svg+xml">Logo icon</object>
                            </a>
                        </div>
                        <div class="offset-xl-2 offset-lg-2 offset-md-1"></div>
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5 col-4 d-flex justify-content-end">
                            <p>Разработано в <a href="https://www.a-lux.kz/"><img src="{{ asset('images/a-lux.png') }}" alt="A-lux"></a></p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    @stack('scripts')
    <script src="{{ mix('js/minor.js') }}"></script>
    <style>
        #catalog section.products .product-card .product-header .tag{
            font-size: 13px !important;
        }
        #home section.products .products-slider .product-card .product-header .tag{
            font-size:13px !important;
        }
        #card .description section.products .products-slider .product-card .product-header .tag{
            font-size: 13px !important;
        }

    </style>
</body>
</html>
