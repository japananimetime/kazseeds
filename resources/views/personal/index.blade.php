@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ mix('css/personal.css') }}">
@endpush

@section('content')
    <div id="personal" class="background-kazseeds"  style="background-position: 50% 50%;')">
            <section class="banner" style="background-image: url({{asset('images/MyAccount1.jpeg')}});filter: grayscale(100%);">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            @if(\Illuminate\Support\Facades\App::getLocale() == 'en')
                                <h1 class="title">My Account</h1>
                            @elseif(\Illuminate\Support\Facades\App::getLocale() == 'ru')
                                <h1 class="title">Мой аккаунт</h1>

                            @elseif(\Illuminate\Support\Facades\App::getLocale() == 'kz')
                                <h1 class="title">Менің аккаунтым</h1>


                            @endif

                        </div>
                    </div>
                </div>
            </section>
        <section class="orders spaced-bottom pt-4">
            <personal-component :lang="`{{\Illuminate\Support\Facades\App::getLocale()}}`"></personal-component>
        </section>
    </div>
@endsection

@push('scripts')
<script src="{{ mix('js/personal.js') }}"></script>
@endpush
