<p>
    Здравствуйте
</p>
<p>
    Вам пришел заказ номер {{$order->id}}
</p>
<p>
    Имя : {{$order->name}}
</p>
<p>
    Телефон : {{$order->phone}}
</p>
<p>
    {{$order->email != null ? 'Почта : '.$order->email : ''}}
</p>
<table>
    <thead>
    <tr>
        <th>
            Продукт
        </th>
        <th>
            Общая цена
        </th>
        <th>
            Количество
        </th>
    </tr>

    </thead>
    <tbody>
    @foreach($order_products as $product)

        <tr>
            <td>
                {{$product->title}}
            </td>
            <td>
                {{$product->total_product_price. ' тг'}}
            </td>
            <td>
                {{$product->quantity.' шт'}}
            </td>

        </tr>
    @endforeach
    <style>
        tr  {
            border:1px solid #000;
        }
        td{
            border:1px solid #000;
        }
    </style>
    </tbody>
</table>
