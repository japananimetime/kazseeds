@extends('layouts.app')

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="forget_title">Поменять пароль</h2>
      </div>
      <div class="col-md-12">
        <update-password :password_validation="`{{ json_encode(__('validation.custom.password')) }}`" :id=`{{ $data->id }}` ></update-password>
      </div>
    </div>
  </div>  
@endsection

<!-- @push('scripts')
<script src="{{ mix('js/updatepass.js') }}"></script>
@endpush -->