import axios from "axios";
import NProgress from "nprogress";
var qs = require("qs");

const apiClient = axios.create({
    baseURL: window.location.origin,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

const token = localStorage.getItem("token");
const config = {
    headers: { Authorization: `Bearer ${token}` }
};

apiClient.interceptors.request.use(config => {
    NProgress.start();
    return config;
});

apiClient.interceptors.response.use(
    response => {
        NProgress.done();
        return response;
    },
    function(error) {
        NProgress.done();
        return Promise.reject(error);
    }
);

export class AuthService {
    static login(data) {
        return apiClient.post("/api/login", data);
    }
    static updatePassword(data) {
        return apiClient.post("/api/update", data);
    }
    static forgetpassword(data) {
        return apiClient.post("/api/forget", data);
    }
    static register(data) {
        return apiClient.post("/api/register", data);
    }
    static emailCheck(email) {
        return apiClient.post("/api/email", { email: email });
    }
    static phoneCheck(phone) {
        return apiClient.post("/api/phone", { phone: phone });
    }
    static authCheck() {
        return apiClient.get("/api/token", config);
    }
}

export class ProductService {
    static getProducts(parametrs) {
        return apiClient.get(`/api/catalog`, { params: parametrs });
    }
}

export class CartService {
    static getProducts(parametrs) {
        return apiClient.get(`/api/basket`, {
            params: parametrs,
            paramsSerializer: params => {
                return qs.stringify(params);
            }
        });
    }
    static checkout(data) {
        return apiClient.post(`/api/order`, data);
    }
}

export class PersonalService {
    static getOrders() {
        return apiClient.get(`/api/order/history`, config);
    }
}

export class TestService {
    static getTest() {
        return apiClient.get(`/api/questions`);
    }
    static answerTest(parametrs) {
        return apiClient.post("/api/test/completed", parametrs);
    }
}

export class FeedbackService {
    static sendRequest(parametrs) {
        return apiClient.post(`/api/request/order`, parametrs);
    }
}
