require("./bootstrap.js");
require("hc-offcanvas-nav");
import AOS from "aos";

const footer = document.getElementById("footer");
let $main_nav = $(".main-nav"),
    $toggle = $(".toggle"),
    defaultOptions = {
        disableAt: 992,
        customToggle: $toggle,
        navTitle: "Меню",
        levelTitles: true,
        levelTitleAsBack: true,
        pushContent: "body > *",
        insertClose: false,
        closeLevels: false
    },
    spaced = document.querySelector(".spaced-bottom");

$main_nav.hcOffcanvasNav(defaultOptions);

if (spaced) {
    spaced.style.paddingBottom = footer.offsetHeight + 30 + "px";
}

window.onscroll = function() {
    if (
        window.innerHeight + window.scrollY >=
        document.body.scrollHeight - (footer.offsetHeight - 35)
    ) {
        footer.classList.add("footer-stick");
    } else {
        footer.classList.remove("footer-stick");
    }
};

AOS.init();
