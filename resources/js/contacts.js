require("./bootstrap.js");
import ymaps from "ymaps";
import Swal from "sweetalert2";
import { FeedbackService } from "./service/service";

window.onload = function() {
    const langitude = document.getElementById("langitude").value;
    const latitude = document.getElementById("latitude").value;
    ymaps
        .load()
        .then(maps => {
            const map = new maps.Map("map", {
                center: [latitude, langitude],
                zoom: 13
            });
            let mark = new maps.Placemark(
                [latitude, langitude],
                {
                    preset: "islands#redDotIcon"
                },
                {}
            );
            map.geoObjects.add(mark);
        })
        .catch(error => console.log("Failed to load Yandex Maps", error));

    const feedbackForm = document.getElementById("feedbackForm");
    feedbackForm.addEventListener("submit", function(e) {
        e.preventDefault();
        let data = new FormData(e.target);
        FeedbackService.sendRequest(data)
            .then(response => {
                Swal.fire({
                    icon: "success",
                    title: response.data.message,
                    showConfirmButton: false,
                    timer: 1500
                });
            })
            .catch(error => {
                console.log(error);
            });
    });
};
