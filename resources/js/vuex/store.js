import Vue from "vue";
import Vuex from "vuex";
import catalog from "./modules/catalog.js";
import personal from "./modules/personal.js";
import cart from "./modules/cart.js";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        catalog,
        personal,
        cart
    }
});
