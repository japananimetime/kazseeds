import {
    AuthService,
    PersonalService,
    CartService
} from "../../service/service.js";

import Swal from "sweetalert2";

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,    
    timerProgressBar: true
});

const state = () => ({
    authenticated: false,
    username: "",
    phone: "",
    email: "",
    user_id: "",
    orders: []
});

const getters = {};

const actions = {
    authCheck({ commit, state }) {
        AuthService.authCheck()
            .then(response => {
                state.authenticated = true;
                commit("SET_USERNAME", response.data.name);
                commit("SET_PHONE", response.data.phone);
                commit("SET_EMAIL", response.data.email);
                commit("SET_USER_ID", response.data.id);
            })
            .catch(error => {
                state.authenticated = false;
            });
    },
    fetchOrders({ commit }) {
        PersonalService.getOrders()
            .then(response => {
                commit("SET_ORDERS", response.data);
            })
            .catch(error => {
                console.log(error);
            });
    },
    increaseQuantity({ commit }, order_id) {
        commit("INCREASE_QUANTITY", order_id);
    },
    decreaseQuantity({ commit }, order_id) {
        commit("DECREASE_QUANTITY", order_id);
    },
    repeatPurchase({ state }, { id, quantity, totalPrice }) {
        let data = new FormData();
        let basket = { id: id, quantity: quantity };
        data.append("basket[]", JSON.stringify(basket));
        data.append("name", state.name);
        data.append("phone", state.phone);
        data.append("user_id", state.user_id);
        data.append("total", totalPrice);
        CartService.checkout(data).then(response => {
            Toast.fire({
                icon: "success",
                title: response.data
            });
        });
    }
};

const mutations = {
    SET_USERNAME(state, username) {
        state.username = username;
    },
    SET_PHONE(state, phone) {
        state.phone = phone;
    },
    SET_EMAIL(state, email) {
        state.email = email;
    },
    SET_USER_ID(state, user_id) {
        state.user_id = user_id;
    },
    SET_ORDERS(state, orders) {
        state.orders = orders;
    },
    INCREASE_QUANTITY(state, order_id) {
        let index = state.orders.findIndex(order => order.id == order_id);
        if (index > -1) {
            state.orders[index].quantity++;
            state.orders[index].total_price += state.orders[index].price;
        }
    },
    DECREASE_QUANTITY(state, order_id) {
        let index = state.orders.findIndex(order => order.id == order_id);
        if (index > -1 && state.orders[index].quantity > 1) {
            state.orders[index].quantity--;
            state.orders[index].total_price -= state.orders[index].price;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
