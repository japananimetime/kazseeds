import { CartService } from "../../service/service.js";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true
});

const state = () => ({
    products: [],
    selectedProducts: [],
    totalPrice: 0
});

const getters = {};

const actions = {
    fetchProducts({ commit }, parametrs) {
        CartService.getProducts(parametrs).then(response => {
            commit("SET_PRODUCTS", response.data);
            commit("SET_SELECTEDPRODUCTS", response.data);
            commit("SET_TOTALPRICE");
        });
    },
    setTotalPrice({ commit }) {
        commit("SET_TOTALPRICE");
    },
    removeProductFromCart({ commit }, id) {
        commit("REMOVE_PRODUCT", id);
        commit("SET_TOTALPRICE");
    },
    increaseQuantity({ commit }, id) {
        commit("INCREASE_QUANTITY", id);
        commit("SET_TOTALPRICE");
    },
    decreaseQuantity({ commit }, id) {
        commit("DECREASE_QUANTITY", id);
        commit("SET_TOTALPRICE");
    },
    checkout({ commit, state }, data) {
        let basketStorage = JSON.parse(localStorage.getItem("basket"));
        for (const index in basketStorage) {
            if (state.selectedProducts[index].is_selected) {
                data.append("basket[]", JSON.stringify(basketStorage[index]));
            }
        }
        data.append("total", state.totalPrice);
        data.append("user_id", localStorage.getItem("user_id"));
        CartService.checkout(data)
            .then(response => {
                let newBasketStorage = basketStorage.filter(
                    (value, index, arr) => {
                        if (!state.selectedProducts[index].is_selected) {
                            return value;
                        }
                    }
                );
                localStorage.setItem(
                    "basket",
                    JSON.stringify(newBasketStorage)
                );
                Toast.fire({
                    icon: "success",
                    title: response.data
                }).then(() => {
                    window.location.replace("/products");
                });
            })
            .catch(error => {
                console.log(error);
            });
    }
};

const mutations = {
    SET_PRODUCTS(state, products) {
        state.products = products;
    },
    SET_SELECTEDPRODUCTS(state, products) {
        for (const product of products) {
            state.selectedProducts.push({ id: product.id, is_selected: true });
        }
    },
    REMOVE_PRODUCT(state, id) {
        let basketStorage = JSON.parse(localStorage.getItem("basket"));
        let index = state.products.findIndex(product => product.id == id);
        if (index > -1) {
            state.products.splice(index, 1);
            basketStorage.splice(index, 1);
        }
        localStorage.setItem("basket", JSON.stringify(basketStorage));
    },
    INCREASE_QUANTITY(state, id) {
        let basketStorage = JSON.parse(localStorage.getItem("basket"));
        let index = state.products.findIndex(product => product.id == id);
        state.products[index].quantity++;
        basketStorage[index].quantity++;
        localStorage.setItem("basket", JSON.stringify(basketStorage));
    },
    DECREASE_QUANTITY(state, id) {
        let basketStorage = JSON.parse(localStorage.getItem("basket"));
        let index = state.products.findIndex(product => product.id == id);
        if (state.products[index].quantity > 1) {
            state.products[index].quantity--;
        }
        if (basketStorage[index].quantity > 1) {
            basketStorage[index].quantity--;
            localStorage.setItem("basket", JSON.stringify(basketStorage));
        }
    },
    SET_TOTALPRICE(state) {
        let sum = 0;
        for (const index in state.products) {
            if (state.selectedProducts[index].is_selected) {
                sum +=
                    state.products[index].price *
                    state.products[index].quantity;
            }
        }
        state.totalPrice = sum;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
