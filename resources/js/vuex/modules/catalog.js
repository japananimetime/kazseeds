import { ProductService } from "../../service/service.js";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true
});

const state = () => ({
    products: [],
    categories: [],
    totalPages: null,
    parametrs: {}
});

const getters = {};

const actions = {
    fetchProducts({ commit }, parametrs) {
        ProductService.getProducts(parametrs).then(response => {
            commit("SET_PRODUCTS", response.data.products.data);
            commit("SET_CATEGORIES", response.data.categories);
            commit("SET_TOTALPAGES", response.data.products.last_page);
        });
    },
    sortByCategory({ commit, state }, category_id) {
        delete state.parametrs.subcategory_id;
        state.parametrs.category_id = category_id;
        state.parametrs.page = 1;
        ProductService.getProducts(state.parametrs).then(response => {
            commit("SET_PRODUCTS", response.data.products.data);
            commit("SET_TOTALPAGES", response.data.products.last_page);
        });
    },
    sortBySubcategory({ commit, state }, subcategory_id) {
        delete state.parametrs.category_id;
        state.parametrs.subcategory_id = subcategory_id;
        state.parametrs.page = 1;
        ProductService.getProducts(state.parametrs).then(response => {
            commit("SET_PRODUCTS", response.data.products.data);
            commit("SET_TOTALPAGES", response.data.products.last_page);
        });
    },
    sortBy({ commit, state }, sorter) {
        if (sorter == "title") {
            delete state.parametrs.sortRule;
            state.parametrs.sortBy = sorter;
        } else {
            delete state.parametrs.sortBy;
            state.parametrs.sortRule = sorter;
        }
        ProductService.getProducts(state.parametrs).then(response => {
            commit("SET_PRODUCTS", response.data.products.data);
        });
    },
    showBy({ commit, state }, quantity) {
        state.parametrs.count = quantity;
        ProductService.getProducts(state.parametrs).then(response => {
            commit("SET_PRODUCTS", response.data.products.data);
            commit("SET_TOTALPAGES", response.data.products.last_page);
        });
    },
    addToCart({ commit }, { product_id, success_msg, warning_msg }) {
        let data = [];
        data.push({
            quantity: 1,
            id: product_id
        });
        if (localStorage.getItem("basket") == null) {
            localStorage.setItem("basket", JSON.stringify(data));
            Toast.fire({
                icon: "success",
                title: success_msg
            });
        } else {
            let index = JSON.parse(localStorage.getItem("basket")).findIndex(
                object => object.id == product_id
            );
            if (index == -1) {
                let basketStorage = JSON.parse(localStorage.getItem("basket"));
                basketStorage.push({ quantity: 1, id: product_id });
                localStorage.setItem("basket", JSON.stringify(basketStorage));
                Toast.fire({
                    icon: "success",
                    title: success_msg
                });
            } else {
                Toast.fire({
                    icon: "warning",
                    title: warning_msg
                });
            }
        }
    },
    paginate({ commit, state }, page) {
        state.parametrs.page = page;
        ProductService.getProducts(state.parametrs).then(response => {
            commit("SET_PRODUCTS", response.data.products.data);
        });
    }
};

const mutations = {
    SET_PRODUCTS(state, products) {
        state.products = products;
    },
    SET_CATEGORIES(state, categories) {
        state.categories = categories;
    },
    SET_TOTALPAGES(state, totalPages) {
        state.totalPages = totalPages;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
