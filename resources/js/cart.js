import Vue from "vue";
import VueRouter from "vue-router";
import VModal from "vue-js-modal";
import Vuellidate from "vuelidate";
import VueTheMask from "vue-the-mask";
import store from "./vuex/store.js";
import PersonalLink from "./components/links/PersonalLink.vue";
import CartComponent from "./components/CartComponent.vue";

Vue.use(VueRouter);
Vue.use(VModal);
Vue.use(Vuellidate);
Vue.use(VueTheMask);

Vue.component("personal-link", PersonalLink);
Vue.component("cart-component", CartComponent);
import Cart from "./views/Cart.vue";
import Feedback from "./views/Feedback.vue";

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/cart/*",
            name: "Cart",
            component: Cart,
            props: true
        },
        {
            path: "/cart/feedback",
            name: "Feedback",
            component: Feedback,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.cart.products.length) {
                    next();
                } else {
                    window.location.replace(window.location.origin);
                }
            }
        }
    ]
});

const app = new Vue({
    el: "#app",
    router,
    store
});
