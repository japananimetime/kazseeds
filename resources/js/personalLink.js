import Vue from "vue";
import VModal from "vue-js-modal";
import Vuellidate from "vuelidate";
import VueTheMask from "vue-the-mask";
import store from "./vuex/store.js";
import PersonalLink from "./components/links/PersonalLink.vue";
import TestComponent from "./components/TestComponent.vue";
import { CollapsePlugin } from "bootstrap-vue";

Vue.use(CollapsePlugin);

Vue.use(VModal);
Vue.use(Vuellidate);
Vue.use(VueTheMask);

Vue.component("personal-link", PersonalLink);
Vue.component("test-component", TestComponent);
const app = new Vue({
    el: "#app",
    store
});
