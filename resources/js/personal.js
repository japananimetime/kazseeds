import Vue from "vue";
import VModal from "vue-js-modal";
import Vuellidate from "vuelidate";
import store from "./vuex/store.js";
import PersonalLink from "./components/links/PersonalLink.vue";
import PersonalComponent from "./components/PersonalComponent.vue";

Vue.use(VModal);
Vue.use(Vuellidate);

Vue.component("personal-link", PersonalLink);
Vue.component("personal-component", PersonalComponent);

const app = new Vue({
    el: "#app",
    store
});
