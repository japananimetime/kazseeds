require("./bootstrap.js");
require("slick-carousel");

$(".main-slider").slick({
    lazyLoad: "ondemand",
    infinite: true,
    dots: true,
    arrows: false,
    autoplay: false,
    speed: 800,
    slidesToShow: 1,
    slidesToScroll: 1,
    rows: 0,
    responsive: [
        {
            breakpoint: 768,
            settings: {
                dots: false,
                arrows: false
            }
        }
    ]
});

let $productSlider, $newsSlider;

$productSlider = $(".products-slider");
$newsSlider = $(".news-slider");

$productSlider.slick({
    lazyLoad: "ondemand",
    slidesToShow: 4,
    slidesToScroll: 1,
    swipeToSlide: true,
    infinite: true,
    autoplay: false,
    dots: false,
    prevArrow: $(".products .prev"),
    nextArrow: $(".products .next"),
    rows: 0,
    centerMode: true,
    centerPadding: "100px",
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                centerPadding: "50px"
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                centerPadding: "50px"
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                centerPadding: "30px"
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                centerPadding: "30px"
            }
        }
    ]
});

$newsSlider.slick({
    lazyLoad: "ondemand",
    slidesToShow: 4,
    slidesToScroll: 1,
    swipeToSlide: true,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 3000,
    dots: false,
    prevArrow: $(".news .prev"),
    nextArrow: $(".news .next"),
    rows: 0,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});
