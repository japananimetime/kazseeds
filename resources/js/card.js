require("./bootstrap.js");
require("slick-carousel");
import Swal from "sweetalert2";
import magnificPopup from "magnific-popup";

$(".slider-for").slick({
    lazyLoad: "ondemand",
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: ".slider-nav",
    rows: 0,
    infinite: true
});

$(".slider-nav").slick({
    lazyLoad: "ondemand",
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: ".slider-for",
    dots: false,
    arrows: false,
    focusOnSelect: true,
    rows: 0,
    infinite: true,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 3
            }
        }
    ]
});

let $productSlider = $(".products-slider");

$productSlider.slick({
    lazyLoad: "ondemand",
    slidesToShow: 4,
    slidesToScroll: 1,
    swipeToSlide: true,
    infinite: true,
    autoplay: false,
    dots: false,
    prevArrow: $(".products .prev"),
    nextArrow: $(".products .next"),
    rows: 0,
    centerMode: 0,
    centerPadding: "100px",
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                centerPadding: "50px"
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                centerPadding: "50px"
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                centerPadding: "30px"
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                centerPadding: "30px"
            }
        }
    ]
});

$(".popup-gallery").magnificPopup({
    delegate: "a",
    type: "image",
    tLoading: "Loading image #%curr%...",
    mainClass: "mfp-img-mobile",
    gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
    }
});

const increaseBtn = document.querySelector(".increase");
const decreaseBtn = document.querySelector(".decrease");
const quantityField = document.querySelector(".quantity-controls input");
const cartBtn = document.querySelector(".cart-btn");
const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

decreaseBtn.addEventListener("click", e => {
    let step = 1;
    if (quantityField.value > step) {
        quantityField.value = quantityField.value - step;
    }
});
increaseBtn.addEventListener("click", e => {
    let step = 1;
    quantityField.value = parseInt(quantityField.value) + step;
});

cartBtn.addEventListener("click", e => {
    console.log(JSON.parse(localStorage.getItem('basket')))
    let data = [];
    data.push({
        quantity: quantityField.value,
        id: e.target.getAttribute("product-id")
    });
    let product = {
        quantity: quantityField.value,
        id: e.target.getAttribute("product-id")
    }

    if (localStorage.getItem("basket") == null) {

        localStorage.setItem("basket", JSON.stringify(data));
        Toast.fire({
            icon: "success",
            title: e.target.getAttribute("data-success")
        });
    } else {

        let index = JSON.parse(localStorage.getItem("basket")).findIndex(
            object =>
                object.id == e.target.getAttribute("product-id") &&
                object.quantity == quantityField.value
        );

        if (index == -1) {
            let basket = JSON.parse(localStorage.getItem("basket"))
            basket.push(product)

            localStorage.setItem("basket", JSON.stringify(basket));
            Toast.fire({
                icon: "success",
                title: e.target.getAttribute("data-success")
            });
        } else {
            Toast.fire({
                icon: "warning",
                title: e.target.getAttribute("data-warning")
            });
        }
    }
});
