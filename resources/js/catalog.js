import Vue from "vue";
import VModal from "vue-js-modal";
import Vuellidate from "vuelidate";
import VueTheMask from "vue-the-mask";
import { CollapsePlugin } from "bootstrap-vue";
import Paginate from "vuejs-paginate";
import store from "./vuex/store.js";
import PersonalLink from "./components/links/PersonalLink.vue";
import CatalogComponent from "./components/CatalogComponent.vue";

Vue.use(VModal);
Vue.use(CollapsePlugin);
Vue.use(Vuellidate);
Vue.use(VueTheMask);

Vue.component("personal-link", PersonalLink);
Vue.component("catalog-component", CatalogComponent);
Vue.component("paginate", Paginate);

const app = new Vue({
    el: "#app",
    store
});
