import Vue from "vue";
import store from "./vuex/store.js";
import UpdatePassword from "./components/UpdatePassword.vue";
import Vuellidate from "vuelidate";

Vue.use(Vuellidate);
Vue.component("update-password", UpdatePassword);

const app = new Vue({
    el: "#app",
    store
});
