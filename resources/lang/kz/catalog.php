<?php
return [
    'product' => [
        'filter' => 'Өнімдер',
        'sortTitle' => 'бойынша сұрыптау',
        'showTitle' => 'бойынша көрсету',
        'sortByName' => 'аты бойынша',
        'sortByPriceAsc' => 'FAO өсуі',
        'sortByPriceDesc' => 'FAO төмендеуі',
        'more' => 'Толығырақ'
    ]
];
