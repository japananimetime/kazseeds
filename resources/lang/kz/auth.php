<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Бұл тіркелгі деректері біздің жазбаларымызға сәйкес келмейді.',
    'throttle' => 'Кіру әрекеттері тым көп. Әрекетті қайталаңыз: секундтар.',
    'auth' => 'Авторизация',
    'registration' => 'Тіркеу',
    'login' => 'Кіру',
    'register' => 'Тіркелу',
    'password' => 'Құпия сөзді еңгізіңіз',
    'name' => 'Аты',
    'phone' => 'Телефон',
    'conf_pass' => 'Құпия сөзді қайта енгізіңіз',
    'submit_login' => 'Кіру',
    'forgot' => 'Құпия сөзді ұмыттыңыз ба?',
    'enter_password' => 'Құпия сөзді еңгізіңіз',
    'enter_email' => 'Электрондық поштаңызды енгізіңіз',
    'submit_register' => 'Тіркелу',
    'reset_password' => 'Құпия сөзді қалпына келтіріңіз',
    'reset' => 'Қалпына келтіру'
];
