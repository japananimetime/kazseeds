<?php
return [
    'product' => [
        'filter' => 'Продукты',
        'sortTitle' => 'сортировать по',
        'showTitle' => 'показывать по',
        'sortByName' => 'по имени',
        'sortByPriceAsc' => 'FAO по возрастанию',
        'sortByPriceDesc' => 'FAO по убыванию',
        'more' => 'Посмотреть больше'
    ]
];
