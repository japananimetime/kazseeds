<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Логин или пароль не найдены.',
    'throttle' => 'Слишком много попыток входа. Попробуйте позже.',
    'auth' => 'Авторизация',
    'registration' => 'Регистрация',
    'login' => 'Войти',
    'register' => 'Зарегистрироваться',
    'password' => 'Введите пароль',
    'name' => 'Имя',
    'phone' => 'Телефон',
    'conf_pass' => 'Пожалуйста введите повторно пароль',
    'submit_login' => 'Войти',
    'forgot' => 'Забыли пароль?',
    'enter_password' => 'Введите пароль',
    'enter_email' => 'Введите email',
    'submit_register' => 'Зарегистрироваться',
    'reset_password' => 'Сброс пароля',
    'reset' => 'Сброс'

];
