<?php

return [
    'product' => 'Продукты',
    'advantages' => 'Преимущества',
    'about' => 'О нас',
    'phone' => 'Телефон',
    'address' => 'Адрес',
    'email' => 'Почта',
    'catalog' => 'Каталог',
    'contact' => 'Контакты',
    'mission' => 'Миссия',
    'we_work' => 'Как мы работаем',
    'write'=> 'Напишите нам',
    'form_name' => 'Введите свое имя',
    'contact_us' => 'Связаться с нами',
    'more' => 'Больше деталей'

];
