<?php

return [
    'product' => 'Products',
    'advantages' => 'Advantages',
    'about' => 'About us',
    'phone' => 'Phone',
    'address' => 'Address',
    'email' => 'E-mail',
    'catalog' => 'Catalog',
    'contact' => 'Contacts',
    'mission' => 'Mission',
    'we_work' => 'How we work',
    'write' => 'Write to us',
    'contact_us' => 'Contact Us',
    'more' => 'More details',
];
