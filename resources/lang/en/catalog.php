<?php
return [
    'product' => [
        'filter' => 'Products',
        'sortTitle' => 'sort by',
        'showTitle' => 'show',
        'sortByName' => 'by name',
        'sortByPriceAsc' => 'FAO ascending',
        'sortByPriceDesc' => 'FAO descending',
        'more' => 'View more'
    ],
];
