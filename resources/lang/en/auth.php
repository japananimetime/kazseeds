<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'auth' => 'Authorization',
    'registration' => 'Registration',
    'login' => 'Login',
    'register' => 'Register',
    'password' => 'Enter password',
    'name' => 'Name',
    'phone' => 'Phone',
    'conf_pass' => 'Please re-enter your password',
    'submit_login' => 'Submit',
    'forgot'  => 'Forgot password?',
    'enter_password' => 'Enter password',
    'enter_email' => 'Enter email',
    'submit_register' => 'Register',
    'reset_password' => 'Reset password',
    'reset' => 'Reset'


];
