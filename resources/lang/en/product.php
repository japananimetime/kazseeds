<?php
return [
    'success' => 'Product is added to cart',
    'warning' => 'Product is already in cart',
    'back' => 'Back to previous page',
    'quantity' => 'Number',
    'buy' => 'Buy product',
    'genetics' => 'Information about genetics/maturity',
    'agronomic' => 'Agronomic characteristics',
    'cob' => 'Characteristics of the cob and grain',
    'introduction' => 'Introduction guide',
    'featured' => 'Featured Products',

];
